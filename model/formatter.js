sap.ui.define([], function() {
	"use strict";
	return {

		onChangeEnabled: function(sStat) {
			if (sStat === "E0022") {
				return true;
			} else {
				return false;
			}
		},

		onChangeNotEnabled: function(sStat) {
			if (sStat === "E0022") {
				return false;
			} else {
				return true;
			}
		},

		onChangeState: function(sStat) {
			if (sStat === "E0022") {
				return true;
			} else {
				return false;
			}
		},

		// onEditChange : function (sStat,sEdit) {
		// 	if (sEdit === "X") {
		// 		return true;
		// 	} else {
		// 		return false;
		// 	}
		// },

		onMalIconVisible: function(sStat) {
			if (sStat === "E0033") {
				return false;
			} else {
				return true;
			}
		},

		onConfirmVisible: function(sStat, sEditable) {
			if ((sStat === "E0022" || sStat === "E0033") && (sEditable === "X")) {
				return true;
			} else {
				return false;
			}
		},

		onEditVisible: function(sEditable) {
			if (sEditable === "X") {
				return true;
			} else {
				return false;
			}
		},

		onDemoVisible: function(sStat, sEditable) {
			if (sStat === "E0029" && sEditable === "X") {
				return true;
			} else {
				return false;
			}
		},

		onDemoFinishVisible: function(sStat, sEditable) {
			if (sStat === "E0031" && sEditable === "X") {
				return true;
			} else {
				return false;
			}
		},

		onIadeIsEmriVisible: function(sStat, sEditable) {
			if (sStat === "E0032" && sEditable === "X") {
				return true;
			} else {
				return false;
			}
		},

		onKiralamaVisible: function(sTedarik) {
			if (sTedarik === "01") {
				return true;
			} else {
				return false;
			}
		},
		
		onCalismaSaatiEdit : function (sKey) {
			if (sKey === "01") {
				return true;
			} else {
				return false;
			}
		}

	};
});