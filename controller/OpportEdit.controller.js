sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"borusan/demo/model/models",
	"borusan/demo/model/formatter"
], function(Controller, JSONModel, History, Filter, FilterOperator, MessageBox, MessageToast, models, formatter) {
	"use strict";

	return Controller.extend("borusan.demo.controller.OpportEdit", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf borusan.demo.view.OpportEdit
		 */
		formatter: formatter,

		onInit: function() {

			var oView = this.getView();
			var oComp = this.getOwnerComponent();
			oView.addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);
		},

		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},
		_onRouteMatched: function(e) {
			var sRouteName = e.getParameter("name");
			if (sRouteName === "opportedit") {
				//var sObjectGuid = e.getParameter("arguments").objectguid;
				this._onResetMaliyetBinding();
				this._onGetCopyDisplay();
				this._getShValues(this);
			} else {
				//this._goBack();
			}
		},
		_onGetCopyDisplay: function() {
			var oDisplay = this.getView().getModel("opportDisplay");
			var oEdit = this.getView().getModel("opportEdit");
			oEdit.setData(oDisplay.getData());
			this._onResetNotesBinding();
		},
		onNavBackToDisplayPage: function() {
			this._goBack();
		},
		_getShValues: function(oController) {
			var oDataModel = oController.getOwnerComponent().getModel();
			var oEditModel = oController.getOwnerComponent().getModel("opportEdit");
			oController._getStatusList(oDataModel, oEditModel, "ZDEM");
			oController._getNoteTypeList(oDataModel, oEditModel, "ZDEM");

			oController._getDomainValues(oDataModel, oEditModel, "/TalepModelList", {
				FieldName: "TALEPEDILENMODE"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/YakitOdeyenList", {
				FieldName: "YAKITODEYEN"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/NakliyeGidisList", {
				FieldName: "NAKLIYE_GIDIS"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/NakliyeDonusList", {
				FieldName: "NAKLIYE_DONUS"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/AplikasyonList", {
				FieldName: "APLIKASYON"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/TedarikLokasyonList", {
				FieldName: "TEDARIKLOKASYON"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/TedarikSekliList", {
				FieldName: "TEDARIKSEKLI"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/TedarikKaynagiList", {
				FieldName: "TEDARIKKAYNAGI"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/DemoNedeniList", {
				FieldName: "DEMONEDENI"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/EgitimOperatoruList", {
				FieldName: "EGITIMOPERATORU"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/MakinaninDonYerList", {
				FieldName: "MAKINANINDONYER"
			});
			oController._getDomainValues(oDataModel, oEditModel, "/SegmentList", {
				FieldName: "SEGMENT"
			});

			oController._getDomainValues(oDataModel, oEditModel, "/MaliyetTypeList", {
				FieldName: "MALIYETTIP"
			});

			oController._getDomainValues(oDataModel, oEditModel, "/MaliyetBirimList", {
				FieldName: "CURRENCY"
			});
		},

		_getStatusList: function(oDataModel, oJsonModel, sProcessType) {
			var aFilters = [];
			var oProcessTypeFilter = new Filter({
				path: 'ProcessType',
				operator: FilterOperator.EQ,
				value1: sProcessType
			});
			aFilters.push(oProcessTypeFilter);
			oDataModel.read("/CustomizingStatusListSet", {
				filters: aFilters,
				success: function(resp) {
					oJsonModel.setProperty("/StatusListSet", resp.results);
				},
				error: function(err) {}
			});
		},

		_getNoteTypeList: function(oDataModel, oJsonModel, sProcessType) {
			var aFilters = [];
			var oProcessTypeFilter = new Filter({
				path: 'ProcessType',
				operator: FilterOperator.EQ,
				value1: sProcessType
			});
			aFilters.push(oProcessTypeFilter);
			oDataModel.read("/CustomizingNoteCategorySHSet", {
				filters: aFilters,
				success: function(resp) {
					oJsonModel.setProperty("/NoteTypeList", resp.results);
				},
				error: function(err) {}
			});
		},

		_getDomainValues: function(oDataModel, oJsonModel, sPropertyName, mParameters) {
			var aFilters = [];
			if (mParameters.FieldName) {
				var oFieldNameFilter = new Filter({
					path: "FieldName",
					operator: FilterOperator.EQ,
					value1: mParameters.FieldName
				});
				aFilters.push(oFieldNameFilter);
			}
			if (mParameters.FieldName === "TEDARIKLOKASYON") {
				var oEdit = this.getView().getModel("opportEdit");
				var sSegment = oEdit.getProperty("/Segment");
				var oTedarik = new Filter({
					path: "Segment",
					operator: FilterOperator.EQ,
					value1: sSegment
				});
				aFilters.push(oTedarik);
			}
			oDataModel.read("/CustomizingDomainSHSet", {
				filters: aFilters,
				success: function(resp) {
					resp.results.unshift({});
					oJsonModel.setProperty(sPropertyName, resp.results);
				},
				error: function(err) {}
			});
		},
		onDataUpdate: function() {
			var that = this;
			var sConfirmText = this.getView().getModel("i18n").getProperty("DEMO_UPDATE_WARNING");
			var sConfirmTitle = this.getView().getModel("i18n").getProperty("DEMO_UPDATE_TITLE");
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.confirm(sConfirmText, {
				title: sConfirmTitle,
				styleClass: bCompact ? "sapUiSizeCompact" : "",
				onClose: function(oEvent) {
					if (oEvent && oEvent === "OK") {

						// var oUpdate = {};
						var oEditModel = that.getView().getModel("opportEdit");

						if (oEditModel) {
							var oEditModelData = oEditModel.getData();
							var oData = that._getDatasFromEditModelData(oEditModelData);
							// oData.Mode = "B";

							oData.NOTES = [];
							if (oEditModelData.NOTES) {
								jQuery.each(oEditModelData.NOTES.results, function(key, el) {
									if (el.Mode != "D") {

										if (el.HeaderGuid) {
											el.Mode = "B";
										} else {
											el.Mode = "A";
										}
									}
									if (el.TextObjectId !== "ZDEM") {
										oData.NOTES.push(el);
									}
								});
							}

							oData.APPOINTMENT = [];

							if (oEditModelData.APPOINTMENT) {
								jQuery.each(oEditModelData.APPOINTMENT.results, function(key, el) {
									var oAppointment = {
										ApptType: el.ApptType + "",
										Mode: "A"
									};
									if (el.HeaderGuid == oData.Guid) {
										oAppointment.HeaderGuid = el.HeaderGuid;
										oAppointment.Mode = "B";
									}

									if (el.DateFrom) {
										oAppointment.DateFrom = new Date(el.DateFrom.getTime() - (el.DateFrom.getTimezoneOffset() * 60 * 1000));
									}
									if (el.TimeFrom) {
										oAppointment.TimeFrom = el.TimeFrom;
									}

									oData.APPOINTMENT.push(oAppointment);
								});
							}

							oData.MALIYET = [];
							if (oEditModelData.MALIYET) {
								jQuery.each(oEditModelData.MALIYET.results, function(key, el) {
									if (el.GerceklesenMaliyet.length === 0) {
										el.GerceklesenMaliyet = "0";
									} else 
									if (el.BeklenenMaliyet.length === 0 ) {
										el.BeklenenMaliyet = "0";
									}
									oData.MALIYET.push(el);
								});
							}

							// var sMessage = this._validateDatas(oData, this.getOwnerComponent().getModel("i18n"));
							// if (sMessage) {
							// 	MessageBox.show(sMessage, {
							// 		icon: "WARNING",
							// 		title: "WARNING"
							// 	});
							// } else {
							that._callCreateService(oData, that);
							// }
						}

					}
				}
			});
		},

		_callCreateService: function(oData, oController) {
			var oDataModel = oController.getView().getModel();
			var oI18nModel = oController.getView().getModel("i18n");
			if (oDataModel && oI18nModel) {
				MessageToast.show(oI18nModel.getProperty("UPDATE_BEGIN_TEXT"));
				oController.getView().setBusy(true);
				oDataModel.create("/DemoRequestSet", oData, {
					success: function(resp) {
						oController.getView().setBusy(false);
						oController._goToDetailPage(resp.Guid);

						var oMasterModel = oController.getOwnerComponent().getModel("Master");
						if (oMasterModel) {
							oMasterModel.setProperty("/DoUpdate", true);
						}
					},
					error: function(err) {
						oController.getView().setBusy(false);
						var sMessage = "";
						var aErrorDetails = JSON.parse(err.responseText).error.innererror.errordetails;
						if (aErrorDetails.length > 0) {
							for (var i = 0; i < aErrorDetails.length; i++) {
								var oError = aErrorDetails[i];
								if (oError.severity === "error") {
									sMessage += (oError.message + "\n");
								}
							}
							if (!sMessage) {
								for (var i = 0; i < aErrorDetails.length; i++) {
									var oError = aErrorDetails[i];
									if (oError.severity === "info") {
										sMessage += (oError.message + "\n");
									}
								}
							}
						} else {
							sMessage = JSON.parse(err.responseText).error.message.value;
						}
						sap.m.MessageBox.error(sMessage, {
							title: oController.getOwnerComponent().getModel("i18n").getProperty("ERROR_TITLE")
						});
					}
				});
			}
		},

		_getDatasFromEditModelData: function(oEditModelData) {
			var oData = {};
			oData.Mode = "B";
			oData.Guid = oEditModelData.Guid;
			oData.PartnerNo = oEditModelData.PartnerNo;
			oData.Description = oEditModelData.Description;
			oData.SerialNo = oEditModelData.SerialNo;
			oData.MakinaOrder = oEditModelData.MakinaOrder;
			oData.PostingDate = oEditModelData.PostingDate;
			oData.RespPssr = oEditModelData.RespPssr;
			oData.ContactPerson = oEditModelData.ContactPerson;
			oData.EgitimOperatoru = oEditModelData.EgitimOperatoru;
			oData.DemoNedeni = oEditModelData.DemoNedeni;
			oData.TalepEdilenMode = oEditModelData.TalepEdilenMode;
			oData.NakliyeGidis = oEditModelData.NakliyeGidis;
			oData.NakliyeDonus = oEditModelData.NakliyeDonus;
			oData.Aplikasyon = oEditModelData.Aplikasyon;
			oData.TedarikSekli = oEditModelData.TedarikSekli;
			oData.TedarikKaynagi = oEditModelData.TedarikKaynagi;
			oData.TedarikLokasyon = oEditModelData.TedarikLokasyon;
			oData.PartnerAddrnumber = oEditModelData.PartnerAddrnumber;
			oData.MakinaninDonYer = oEditModelData.MakinaninDonYer;
			oData.AylikCalismaSaati = oEditModelData.AylikCalismaSaati;
			oData.TrainingOpe = oEditModelData.TrainingOpe;

			return oData;
		},

		_getPartnerDialog: function() {
			if (!this._partnerDialogFragment) {
				this._partnerDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.partnerSHDialogEdit", this);
				this._partnerDialogFragment.setModel(new JSONModel(), "partnerModel");
				this.getView().addDependent(this._partnerDialogFragment);
				this._partnerDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			return this._partnerDialogFragment;
		},

		onCustomerSH: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setProperty("/RolType", "CRM000");
			oPartnerModel.setProperty("/PARTNER_DIALOG_TITLE", "M\u00FC\u015Fteri");
			oPartnerModel.setProperty("/PropertyPathToSet1", "/PartnerFullname");
			oPartnerModel.setProperty("/PropertyPathToSet2", "/PartnerNo");
			var oBinding = oPartnerDialog.getBinding("items");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: 'RolType',
				operator: FilterOperator.EQ,
				value1: "CRM000"
			});
			aFilters.push(oQueryFilter);
			oBinding.filter(aFilters);
			oPartnerDialog.open();
		},
		onContactPersonSH: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oEdit = this.getView().getModel("opportEdit");
			var smusteri = oEdit.getProperty("/PartnerNo");

			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setProperty("/RolType", "BUP001");
			oPartnerModel.setProperty("/PARTNER_DIALOG_TITLE", "\u0130lgili Ki\u015Fi");
			oPartnerModel.setProperty("/PropertyPathToSet1", "/ContactPersonName");
			oPartnerModel.setProperty("/PropertyPathToSet2", "/ContactPerson");
			var oBinding = oPartnerDialog.getBinding("items");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: "BUP001"
			});
			aFilters.push(oQueryFilter);

			var oMusterifilter = new Filter({
				path: "CustomerNo",
				operator: FilterOperator.EQ,
				value1: smusteri
			});
			aFilters.push(oMusterifilter);

			oBinding.filter(aFilters);
			oPartnerDialog.open();
		},
		_goBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				this.getView().getModel("demo").setProperty("/ServiceMode", false);
				window.history.go(-1);
			} else {
				var oRouter = this.getRouter();
				oRouter.navTo("master", true);
			}
		},
		onPartnerDialogConfirm: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			var aSelectedItems = oEvent.getParameter("selectedItems");
			if (aSelectedItems && aSelectedItems.length > 0) {
				var oSelectedItem = aSelectedItems[0];
				var oBindingContext = oSelectedItem.getBindingContext();
				var oModel = oBindingContext.getModel();
				var sPath = oBindingContext.getPath();
				var oSelectedData = oModel.getProperty(sPath);
				var oCreateModel = this.getOwnerComponent().getModel("opportEdit");
				oCreateModel.setProperty(oPartnerModel.getProperty("/PropertyPathToSet1"), oSelectedData.PartnerFullname);
				oCreateModel.setProperty(oPartnerModel.getProperty("/PropertyPathToSet2"), oSelectedData.PartnerNo);
				oPartnerModel.setData({});
			}
			this._onSelectAccountAddress(oSelectedData.PartnerNo);
		},
		_onSelectAccountAddress: function(sPartnerNo) {
			var that = this;
			var oModel = this.getView().getModel();
			var oCreate = this.getView().getModel("opportEdit");
			var oFilter = new sap.ui.model.Filter({
				path: "accountID",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: sPartnerNo
			});
			oModel.read("/BPAddressSet", {
				filters: [oFilter],
				success: function(resp) {
					oCreate.setProperty("/AddressCollection", resp.results);
					if (!that.AccountAddressDialog) {
						that.AccountAddressDialog = sap.ui.xmlfragment("borusan.demo.fragments.AccountAddressEdit", that);
						that.AccountAddressDialog.setModel(that.getView().getModel("i18n"), "i18n");
						that.getView().addDependent(that.AccountAddressDialog);
					}
					that.AccountAddressDialog.open();
				},
				errorr: function(err) {
					// TODO: Error Mesaji basilacak
				}
			});
		},
		onAddressSearch: function(e) {
			var sValue = e.getParameter("value");
			var oAddress = new sap.ui.model.Filter("address", sap.ui.model.FilterOperator.Contains, sValue);
			e.getSource().getBinding("items").filter(oAddress);
		},
		onAddressConfirm: function(e) {
			var oCreate = this.getView().getModel("opportEdit");
			var aContexts = e.getParameter("selectedContexts");
			var sAddressNumber = aContexts.map(function(oContext) {
				return oContext.getObject().addressNumber;
			}).join(", ");
			oCreate.setProperty("/PartnerAddrnumber", sAddressNumber);
			if (aContexts && aContexts.length) {
				MessageToast.show("You have chosen " + sAddressNumber);
			} else {
				MessageToast.show("No new item was selected.");
			}
			e.getSource().getBinding("items").filter([]);
		},
		onSerialNoSH: function() {
			var that = this;
			var oEdit = this.getView().getModel("opportEdit");
			var sTedarikKey = oEdit.getProperty("/TedarikSekli");
			if (sTedarikKey.length > 0) {
				var oFilter = new Filter({
					path: 'TedarikSekli',
					operator: FilterOperator.EQ,
					value1: sTedarikKey
				});
				var oModel = this.getView().getModel();
				oModel.read("/CustomizingSerialNoSHSet", {
					filters: [oFilter],
					success: function(resp) {
						oEdit.setProperty("/SerialNoCollection", resp.results);

						if (!that.SerialNoDialog) {
							that.SerialNoDialog = sap.ui.xmlfragment("borusan.demo.fragments.SerialNoDialog", that);
							that.getView().addDependent(that.SerialNoDialog);
							that.SerialNoDialog.addStyleClass(that.getOwnerComponent().getContentDensityClass());
						}
						that.SerialNoDialog.open();
					},
					error: function(err) {
						// TODO: Error  ver 
					}
				});
			} else {
				// TODO: Uyari ver 
			}
		},
		onSerialNoConfirm: function(e) {
			var oEdit = this.getView().getModel("opportEdit");
			var aContexts = e.getParameter("selectedContexts");
			var sSerialNo = aContexts.map(function(oContext) {
				return oContext.getObject().SerialNo;
			}).join(", ");
			var sModel = aContexts.map(function(oContext) {
				return oContext.getObject().Model;
			}).join(", ");
			var sBatchID = aContexts.map(function(oContext) {
				return oContext.getObject().BatchId;
			}).join(", ");
			if (aContexts && aContexts.length) {
				MessageToast.show("You have chosen " + sSerialNo);
			} else {
				MessageToast.show("No new item was selected.");
			}
			e.getSource().getBinding("items").filter([]);
			oEdit.setProperty("/SerialNo", sSerialNo);
			oEdit.setProperty("/Model", sModel);
			oEdit.setProperty("/MakinaOrder", sBatchID);
		},
		onSerialNoSearch: function(e) {
			var sValue = e.getParameter("value");
			var oSerial = new sap.ui.model.Filter("SerialNo", sap.ui.model.FilterOperator.Contains, sValue);
			e.getSource().getBinding("items").filter(oSerial);
		},

		onUpdateCancel: function() {
			var oBundle = this.getView().getModel("i18n").getResourceBundle();
			var sCancelDialogText = oBundle.getText("CREATE_CANCEL_DIALOG_TEXT");
			var sCancelDialogTitle = oBundle.getText("CREATE_CANCEL_DIALOG_TITLE");
			var oController = this;
			MessageBox.confirm(sCancelDialogText, {
				title: sCancelDialogTitle,
				onClose: function(oEvent) {
					if (oEvent && oEvent === "OK") {
						oController._goBack();
					}
				}
			});
		},

		onNoteAddBtnPress: function(oEvent) {
			var oI18nModel = this.getView().getModel("i18n");
			var oNoteDialog = this._getNoteDialog();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			oNoteModel.setProperty("/textSuccessButton", oI18nModel.getProperty("ADD"));
			oNoteModel.setProperty("/NoteDialogTitleText", oI18nModel.getProperty("NOTE_DIALOG_TITLE_CREATE"));

			var oEditModel = this.getOwnerComponent().getModel("opportEdit");
			var aNoteTypeList = oEditModel.getProperty("/NoteTypeList");
			var aSelectableNoteTypeList = [];
			var aNotes = oEditModel.getProperty("/NOTES/results");
			for (var i = 0; i < aNoteTypeList.length; i++) {
				var bControl = true;
				if (aNotes && aNotes.length > 0) {
					for (var j = 0; j < aNotes.length; j++) {
						if (aNoteTypeList[i].Textid === aNotes[j].TextObjectId) {
							bControl = false;
						}
					}
				}
				if (bControl) {
					aSelectableNoteTypeList.push(aNoteTypeList[i]);
				}
			}
			oNoteModel.setProperty("/NoteTypeList", aSelectableNoteTypeList);
			oNoteModel.setProperty("/NoteTypeSelectEnabled", true);
			if (aSelectableNoteTypeList.length > 0) {
				oNoteDialog.open();
			} else {
				MessageToast.show(this.getOwnerComponent().getModel("i18n").getProperty("MAX_LIMIT_OF_NOTES_EXCEEDED_TEXT"));
				oNoteModel.setData({});
			}
		},

		_getNoteDialog: function() {
			if (!this._noteDialogFragment) {
				this._noteDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.noteEditDialog", this);
				this._noteDialogFragment.setModel(new JSONModel(), "noteModel");
				this.getView().addDependent(this._noteDialogFragment);
				this._noteDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			return this._noteDialogFragment;
		},

		onNoteDialogCancel: function(oEvent) {
			var oNoteDialog = this._getNoteDialog();
			oNoteDialog.close();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			oNoteModel.setData({});
		},

		onNoteUpdateBtnPress: function(oEvent) {
			var oI18nModel = this.getView().getModel("i18n");
			var oEditModel = this.getOwnerComponent().getModel("opportEdit");
			var oNoteDialog = this._getNoteDialog();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			var oBindingContext = oEvent.getSource().getBindingContext("opportEdit");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);
			oNoteModel.setProperty("/NoteDialogTitleText", oI18nModel.getProperty("NOTE_DIALOG_TITLE_EDIT"));
			oNoteModel.setProperty("/NoteType", oSelectedNote.TextObjectId);
			oNoteModel.setProperty("/NoteTypeList", oEditModel.getProperty("/NoteTypeList"));
			oNoteModel.setProperty("/NoteTextDialog", oSelectedNote.Content);
			oNoteModel.setProperty("/IsUpdate", true);
			oNoteModel.setProperty("/SPath", sPath);
			oNoteModel.setProperty("/NoteTypeSelectEnabled", false);
			oNoteDialog.open();
		},

		onNoteDeleteBtnPress: function(oEvent) {
			var oParent = oEvent.getSource().getParent();
			var aItems = oParent.getAggregation("items");
			var oController = this;
			var oI18nModel = oController.getView().getModel("i18n");
			var oResourceBundle = oI18nModel.getResourceBundle();
			var oDeleteDialogText = oResourceBundle.getText("DELETE_DIALOG_TEXT", ["id"]);
			var oDeleteDialogTitle = oResourceBundle.getText("DELETE_DIALOG_TITLE");
			var oEditModel = this.getOwnerComponent().getModel("opportEdit");
			var oBindingContext = oEvent.getSource().getBindingContext("opportEdit");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);
			var sModeColor;
			MessageBox.show(oDeleteDialogText, {
				icon: "WARNING",
				title: oDeleteDialogTitle,
				actions: ["OK", "CANCEL"],
				onClose: function(oCloseEvent) {
					if (oCloseEvent && oCloseEvent === "OK") {
						var oNote;
						// if (oSelectedNote.IsUpdate && oSelectedNote.SPath) {
						oNote = oSelectedNote;
						oNote.Mode = "D";

						for (var i = 0; i < 2; i++) {
							if (aItems[i].mProperties.icon === "sap-icon://delete") {
								aItems[i].mProperties.type = "Reject";
								oController.getView().rerender();
							}
						}
						// oSelectedNote.SPath = sPath;

						oEditModel.setProperty(oSelectedNote.SPath, oNote);

						// }
					}
				}
			});
		},

		onNoteDialogSave: function(oEvent) {
			var oNoteDialog = this._getNoteDialog();
			oNoteDialog.close();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			var oCreateModel = oNoteDialog.getModel("opportEdit");
			var oNoteData = oNoteModel.getData();
			if (!oNoteData.NoteTextDialog) {
				MessageToast.show(this.getOwnerComponent().getModel("i18n").getProperty("EMPTY_NOTE_TOAST_TEXT"));
			} else {
				var oNote;
				if (oNoteData.IsUpdate && oNoteData.SPath) {
					oNote = oCreateModel.getProperty(oNoteData.SPath);
					oNote.Content = oNoteData.NoteTextDialog;
					oCreateModel.setProperty(oNoteData.SPath, oNote);
				} else {
					oNote = {};
					oNote.TextObjectIdDesc = oEvent.getSource().getParent().getAggregation("content")[0].getContent()[1].getSelectedItem().getText();
					oNote.Mode = "A";
					oNote.Content = oNoteData.NoteTextDialog;
					oNote.TextObjectId = oNoteData.NoteType;
					var aNotes = oCreateModel.getProperty("/NOTES/results");
					if (!aNotes) {
						aNotes = [];
					}
					aNotes.push(oNote);
					aNotes = oCreateModel.setProperty("/NOTES/results", aNotes);
				}
				oNoteModel.setData({});
			}
		},
		_goToDetailPage: function(sGuid) {
			var oRouter = this.getRouter();
			oRouter.navTo("opportdisplay", {
				objectguid: sGuid
			});
			// this._resetCreatePage();
		},
		onMarginAddBtnPress: function(oEvent) {
			this.getView().setBusy(true);
			var oI18nModel = this.getView().getModel("i18n");
			var oDataModel = this.getView().getModel();
			var oEditModel = this.getOwnerComponent().getModel("opportEdit");
			var oMargin = this.getView().getModel("demo");

			oMargin.setProperty("/MaliyetTip", "");
			oMargin.setProperty("/BeklenenMaliyet", "");
			oMargin.setProperty("/BeklenenMaliyetBirim", "");
			oMargin.setProperty("/GerceklesenMaliyet", "");
			oMargin.setProperty("/GerceklesenMaliyetBirim", "");
			oMargin.setProperty("/MaliyetTypeList", oEditModel.getProperty("/MaliyetTypeList"));
			oMargin.setProperty("/MaliyetBirimList", oEditModel.getProperty("/MaliyetBirimList"));
			var oMarginDialog = this._getMarginDialog();
			oMarginDialog.open();

		},
		_getMarginDialog: function() {

			if (!this._marginDialogFragment) {
				this._marginDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.maliyetEditDialog", this);
				this.getView().addDependent(this._marginDialogFragment);
				this._marginDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			// this._marginDialogFragment.open();
			return this._marginDialogFragment;
		},
		onMarginDialogCancel: function() {
			if (this._marginDialogFragment) {
				this._marginDialogFragment.close();
			}
			this.getView().setBusy(false);

		},
		onMarginDialogSave: function(oEvent) {
			var oMargin = this.getView().getModel("demo");
			var oEditModel = this.getView().getModel("opportEdit");
			var oMarginData = oMargin.getData();

			if (!oMarginData.MaliyetTip) {
				MessageToast.show(this.getOwnerComponent().getModel("i18n").getProperty("EMPTY_MALIYET_TYPE_TEXT"));
			} else {
				var oMarginNew;
				if (oMarginData.IsUpdate && oMarginData.SPath) {
					oMarginNew = oEditModel.getProperty(oMarginData.SPath);
					oMarginNew.MaliyetTip = oMarginData.MaliyetTip;
					oEditModel.setProperty(oMarginData.SPath, oMarginNew);
				} else {
					oMarginNew = {};
					oMarginNew.MaliyetTip = oMarginData.MaliyetTip;
					oMarginNew.BeklenenMaliyet = oMarginData.BeklenenMaliyet;
					oMarginNew.BeklenenMaliyetBirim = oMarginData.BeklenenMaliyetBirim;
					oMarginNew.GerceklesenMaliyet = oMarginData.GerceklesenMaliyet;
					oMarginNew.GerceklesenMaliyetBirim = oMarginData.GerceklesenMaliyetBirim;

					var aMargin = oEditModel.getProperty("/MALIYET/results");
					if (!aMargin) {
						aMargin = [];
					}
					aMargin.push(oMarginNew);
					aMargin = oEditModel.setProperty("/MALIYET/results", aMargin);
				}
				oMargin.setData({});
			}
			if (this._marginDialogFragment) {
				this._marginDialogFragment.close();
				this.getView().setBusy(false);
			}

		},

		onMarginUpdateBtnPress: function(oEvent) {
			var oI18nModel = this.getView().getModel("i18n");
			var oEditModel = this.getOwnerComponent().getModel("opportEdit");
			var oMarginDialog = this._getMarginDialog();
			var oMargin = oMarginDialog.getModel("demo");
			var oBindingContext = oEvent.getSource().getBindingContext("opportEdit");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);

			oMargin.setProperty("/MaliyetTip", oSelectedNote.MaliyetTip);
			oMargin.setProperty("/BeklenenMaliyet", oSelectedNote.BeklenenMaliyet);
			oMargin.setProperty("/BeklenenMaliyetBirim", oSelectedNote.BeklenenMaliyetBirim);
			oMargin.setProperty("/GerceklesenMaliyet", oSelectedNote.GerceklesenMaliyet);
			oMargin.setProperty("/GerceklesenMaliyetBirim", oSelectedNote.GerceklesenMaliyetBirim);
			oMargin.setProperty("/MaliyetTypeList", oEditModel.getProperty("/MaliyetTypeList"));
			oMargin.setProperty("/MaliyetBirimList", oEditModel.getProperty("/MaliyetBirimList"));

			oMargin.setProperty("/IsUpdate", true);
			oMargin.setProperty("/SPath", sPath);
			// oMargin.setProperty("/NoteTypeSelectEnabled", false);

			oMarginDialog.open();
		},

		onMarginDelBtnPress: function(oEvent) {
			var oParent = oEvent.getSource().getParent();
			
			
			var oController = this;
			var oI18nModel = oController.getView().getModel("i18n");
			var oResourceBundle = oI18nModel.getResourceBundle();
			var oDeleteDialogText = oResourceBundle.getText("DELETE_DIALOG_MALIYET", ["id"]);
			var oDeleteDialogTitle = oResourceBundle.getText("DELETE_DIALOG_MALIYET_TITLE");
			var oEditModel = this.getOwnerComponent().getModel("opportEdit");
			var oBindingContext = oEvent.getSource().getBindingContext("opportEdit");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);
			var sDelMode;
			MessageBox.show(oDeleteDialogText, {
				icon: "WARNING",
				title: oDeleteDialogTitle,
				actions: ["OK", "CANCEL"],
				onClose: function(oCloseEvent) {
					if (oCloseEvent && oCloseEvent === "OK") {
						var oMaliyet;
						// if (oSelectedNote.IsUpdate && oSelectedNote.SPath) {
						oMaliyet = oSelectedNote;
						oMaliyet.Mode = "D";
						sDelMode = "Reject";
						oParent.getAggregation("cells")[5].mProperties.type = "Reject";
						oController.getView().rerender();
						oEditModel.setProperty(oSelectedNote.SPath, oMaliyet);
						// }
					} 
				}
			});
		},

		onChangeTime: function(e) {},

		onEgitimOperatoruSH: function(e) {

			if (!this._egitimOperatorFragment) {
				this._egitimOperatorFragment = sap.ui.xmlfragment("borusan.demo.fragments.EgitimOperatoru_VH", this);
				this.getView().addDependent(this._egitimOperatorFragment);
				this._egitimOperatorFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			var oEdit = this.getView().getModel("opportEdit");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: "BUP003"
			});
			aFilters.push(oQueryFilter);

			var oBinding = this._egitimOperatorFragment.getBinding("items");
			oBinding.filter(aFilters);

			this._egitimOperatorFragment.open();
		},

		onOperatorDialogConfirm: function(e) {
			var aSelectedItems = e.getParameter("selectedItems");
			if (aSelectedItems && aSelectedItems.length > 0) {
				var oItem = aSelectedItems[0];
				var oBinding = oItem.getBindingContext();
				var oModel = oBinding.getModel();
				var sPath = oBinding.getPath();
				var oData = oModel.getProperty(sPath);
				var oEdit = this.getView().getModel("opportEdit");
				oEdit.setProperty("/TrainingOpeName", oData.PartnerFullname);
				oEdit.setProperty("/TrainingOpe", oData.PartnerNo);
			}
		},

		onOperatorSearch: function(e) {
			var sQuery = e.getParameter("value");
			var oBinding = e.getSource().getBinding("items");
			var aFilters = [];
			var oRolTypeFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: "BUP003"
			});
			aFilters.push(oRolTypeFilter);
			if (sQuery) {
				var oQueryFilter = new Filter({
					path: "PartnerFullname",
					operator: FilterOperator.EQ,
					value1: sQuery
				});
				aFilters.push(oQueryFilter);
			}
			oBinding.filter(aFilters);
		},
		
		_onResetMaliyetBinding : function () {
			var aItems =  this.byId("idMargin").getItems();
			for (var i=0;i<aItems.length;i++) {
				aItems[i].getCells()[5].setProperty("type","Default");		// Burasi dinamik degil tablo kolon degisince degistirmek gerekli !!!
			}
		},
		
		_onResetNotesBinding : function () {	// Xml degisirse burasi da degismek zorunda Hbox ve FlexBox var 
			var oList = this.byId("idListNotes");
			var aItems = oList.getItems();
			for (var i=0;i<aItems.length;i++) {
				var aContent = aItems[i].getContent();	// Xml'e gore yazildi 
				var oItem = aContent[0].getAggregation("items")[1];
				var oButton = oItem.getAggregation("items")[1];		// Delete button 
				oButton.setProperty("type","Default");
			}
		}

	});

});