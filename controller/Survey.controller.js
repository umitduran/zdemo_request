sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast"
], function(Controller, MessageBox, History, JSONModel, MessageToast) {
	"use strict";

	return Controller.extend("borusan.demo.controller.Survey", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf borusan.demo.view.OpportEdit
		 */

		onInit: function() {

			var oComp = this.getOwnerComponent();
			this.getView().addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);
			this.aSurveyCollection = [];
		},

		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},
		_onRouteMatched: function(e) {
			var sRouteName = e.getParameter("name");
			if (sRouteName === "survey") {
				var sIndex = e.getParameter("arguments").index;
				var oDisplay = this.getView().getModel("opportDisplay");
				var oDemo = this.getView().getModel("demo");
				var sPath = "/SURVEYS/results/" + sIndex;
				var sGuid = oDisplay.getProperty("/Guid");
				var sSurveyID = oDisplay.getProperty(sPath).SurveyId;
				var sSurveyTitle = oDisplay.getProperty(sPath).SurveyIdText;
				oDemo.setProperty("/SurveyTitle",sSurveyTitle);
				oDemo.setProperty("/SurveyId", sSurveyID);
				oDemo.setProperty("/Guid", sGuid);
				oDemo.setProperty("/SurveyUpdEnabled", false);
				this._onRefreshFormContent();
				this._onGetDemoRequestSurvey(sSurveyID, sGuid);
				this.getView().setBusy(true);
			}
		},

		_onGetDemoRequestSurvey: function(sID, sGuid) {
			var that = this;
			var aFilter = [];
			var oFieldName = new sap.ui.model.Filter({
				path: "SurveyId",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: sID
			});
			aFilter.push(oFieldName);
			var oStatus = new sap.ui.model.Filter({
				path: "Guid",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: sGuid
			});
			aFilter.push(oStatus);
			var oModel = this.getView().getModel();
			oModel.read("/DemoRequestSurveySet", {
				filters: aFilter,
				urlParameters: {
					"$expand": "Answers"
				},
				success: function(resp) {
					that.getView().setBusy(false);
					if (resp.results.length > 0) {
						that._onSetFields(resp.results);
						that.aSurveyCollection = resp.results;
					} else {
						var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
						MessageBox.information(
							that.getView().getModel("i18n").getProperty("NO_SURVEY_FOUND"), {
								styleClass: bCompact ? "sapUiSizeCompact" : ""
							}
						);
					}
				},
				errorr: function(err) {
					that._onHandleErrorMsg();
					that.getView().setBusy(false);
				}
			});
		},

		_onSetFields: function(aField) {
			var oSurvey = new JSONModel();
			this.getView().setModel(oSurvey, "survey");
			var oDemo = this.getView().getModel("demo");
			var aElements = [];
			//var that = this;
			var iField = aField.length;
			if (iField > 0) {
				for (var i = 0; i < iField; i++) {
					if (aField[i].Type === "Text") {
						var sQuestionID = aField[i].QuestionId;
						var sAnswerID = aField[i].Answers.results[0].Id;
						oDemo.setProperty("/" + sAnswerID, sQuestionID); // her bir answer icin question id tutuyorum
						var oLabel = new sap.m.Label({
							text: aField[i].QuestionText,
							layoutData: new sap.ui.layout.GridData({span: "XL1 L6 M6 S12"})
						});
						if (aField[i].Answers.results[0].Selected.length === 0) {
							var sPlaceHolder = aField[i].Answers.results[0].Text;
							var sValue = null;
						} else {
							sPlaceHolder = "";
							sValue = aField[i].Answers.results[0].Text;
						}
						var oInput = new sap.m.Input({
							placeholder : sPlaceHolder,
							id: sAnswerID,
							value: sValue,
							change: function(e) {
								//oDemo.setProperty("/" + e.getParameters().id, e.getParameters().value);
								var oText = {
									id: oDemo.getProperty("/" + e.getParameters().id), //QuestionId tutuyorum property name o answerin id ile 
									content: e.getParameters().value,
									answerID: e.getParameters().id
								};
								oSurvey.setProperty("/" + e.getParameters().id, oText);
								oDemo.setProperty("/SurveyUpdEnabled", true);
							}
						});
						aElements.push(oLabel);
						aElements.push(oInput);
					} else
					if (aField[i].Type === "SingleChoice") {
						var iItems = aField[i].Answers.results.length;
						sQuestionID = aField[i].QuestionId;
						var oLabelRadio = new sap.m.Label({
							text: aField[i].QuestionText,
							layoutData: new sap.ui.layout.GridData({span: "XL1 L6 M6 S12"})
						});

						var oRadioGroup = new sap.m.RadioButtonGroup({
							columns: 1, //aField[i].Answers.results.length,
							items: [],
							selectedIndex: -1,
							id: sQuestionID
						});
						for (var j = 0; j < iItems; j++) {
							sAnswerID = aField[i].Answers.results[j].Id;
							oDemo.setProperty("/" + sAnswerID, sQuestionID); // her bir answer icin question id tutuyorum
							var oRadio = new sap.m.RadioButton({
								id: sAnswerID,
								text: aField[i].Answers.results[j].Text,
								groupName: aField[i].Answers.results[j].QuestionId,
								select: function(e) {
									sAnswerID = e.getParameters().id;
									//oDemo.setProperty("/" + sQuestionID, e.getParameters().selected);
									var oSingle = {
										id: oDemo.getProperty("/" + e.getParameters().id),
										content: e.getParameters().selected,
										answerID: e.getParameters().id
									};
									oSurvey.setProperty("/" + sAnswerID, oSingle);
									oDemo.setProperty("/SurveyUpdEnabled", true);
								}
							});
							if (aField[i].Answers.results[j].Selected === "X") {
								oRadio.setSelected(true);
							} else {
								oRadio.setSelected(false);
							}
							oRadioGroup.addButton(oRadio);
						}
						aElements.push(oLabelRadio);
						aElements.push(oRadioGroup);
					} else
					if (aField[i].Type === "MultipleChoice") {
						iItems = aField[i].Answers.results.length;
						sQuestionID = aField[i].QuestionId;

						var oLabelCheck = new sap.m.Label({
							text: aField[i].QuestionText,
							layoutData: new sap.ui.layout.GridData({span: "XL1 L6 M6 S12"})
						});
						aElements.push(oLabelCheck);
						for (var k = 0; k < iItems; k++) {
							sAnswerID = aField[i].Answers.results[k].Id;
							oDemo.setProperty("/" + sAnswerID, sQuestionID); // her bir answer icin question id tutuyorum
							var oCheckBox = new sap.m.CheckBox({
								id: aField[i].Answers.results[k].Id,
								text: aField[i].Answers.results[k].Text,
								select: function(e) {
									sAnswerID = e.getParameters().id;
									//oDemo.setProperty(sQuestionID, e.getParameters().selected);
									var oMulti = {
										id: oDemo.getProperty("/" + e.getParameters().id),
										content: e.getParameters().selected,
										answerID: e.getParameters().id
									};
									oSurvey.setProperty("/" + sAnswerID, oMulti);
									oDemo.setProperty("/SurveyUpdEnabled", true);
								}
							});
							if (aField[i].Answers.results[k].Selected === "X") {
								oCheckBox.setSelected(true);
							} else {
								oCheckBox.setSelected(false);
							}
							aElements.push(oCheckBox);
						}
					} else
					if (aField[i].Type === "Section") {
						var oToolBar = new sap.m.Toolbar({
							content : new sap.m.Title({
								text : aField[i].QuestionText,
								level : "H1",
								titleStyle  : "H5"
							})
						});
						aElements.push(oToolBar);
					}
				}
				var oSimpleForm = new sap.ui.layout.form.SimpleForm({
					id: "idSurveySimple",
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					editable: true,
					labelMinWidth: 60,
					content: aElements,
					BackgroundDesign: "Transparent",
					singleContainerFullSize: true,
					adjustLabelSpan: false
				});
				this.byId("idPageSurvey").addContent(oSimpleForm);
				this.getView().setBusy(false);
			} else {
				// TODO: Form Bosaltilcak 
				this.getView().setBusy(false);
			}
		},

		onUpdateSurvey: function() {
			var sConfirmText = this.getView().getModel("i18n").getProperty("SURVEY_UPDATE_WARNING");
			var sConfirmTitle = this.getView().getModel("i18n").getProperty("SURVEY_UPDATE_TITLE");
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			var oI18nModel = this.getView().getModel("i18n");
			var that = this;
			MessageBox.confirm(sConfirmText, {
				title: sConfirmTitle,
				styleClass: bCompact ? "sapUiSizeCompact" : "",
				onClose: function(oEvent) {
					if (oEvent && oEvent === "OK") {

						that.getView().setBusy(true);
						var aSurvey = []; //Servisden gelen anket 
						var aResult = []; // userin eventlerinin sonuclari
						aSurvey = that.aSurveyCollection;
						var oModel = that.getView().getModel();
						var oDemo = that.getView().getModel("demo");
						var oSurvey = that.getView().getModel("survey");
						aResult = oSurvey.getData();
						var oUpdate = {};
						oUpdate.SurveyId = oDemo.getProperty("/SurveyId");
						var sGuid = oDemo.getProperty("/Guid");
						oUpdate.Guid = sGuid;
						oUpdate.Questions = [];

						for (var i = 0; i < aSurvey.length; i++) { // Compare kismi
							var oQuestion = {};
							oQuestion.Answers = [];
							oQuestion.AnswerId = aSurvey[i].AnswerId;
							oQuestion.QuestionId = aSurvey[i].QuestionId;
							oQuestion.Type = aSurvey[i].Type;
						
							for (var j = 0; j < aSurvey[i].Answers.results.length; j++) {

								jQuery.each(aResult, function(key, el) {
									if (aSurvey[i].Answers.results[j].Id === el.answerID) {
										if (aSurvey[i].Type !== "Text") {
											if (el.content) {
												aSurvey[i].Answers.results[j].Selected = "X";
											} else {
												aSurvey[i].Answers.results[j].Selected = "";
											}
										} else {
											aSurvey[i].Answers.results[j].Text = el.content;
										}
									}
								});
								delete aSurvey[i].Answers.results[j].__metadata;
							}
							delete aSurvey[i].__metadata;
							oQuestion.Answers = aSurvey[i].Answers.results;
							oUpdate.Questions.push(oQuestion);
						}

						oModel.create("/DemoRequestQuestionsSet", oUpdate, {
							success: function(resp) {
								that.getView().setBusy(false);
								that._onRefreshFormContent();
								MessageToast.show(oI18nModel.getProperty("SURVEY_UPDATED"));
								that._goToDetailPage(sGuid);
							},
							error: function(err) {
								that.getView().setBusy(false);
								that._onHandleErrorMsg(err);
							}
						});

					}
				}
			});
		},

		onUpdateCancel: function() {
			var oBundle = this.getView().getModel("i18n").getResourceBundle();
			var sCancelDialogText = oBundle.getText("SURVEY_CANCEL_DIALOG_TEXT");
			var sCancelDialogTitle = oBundle.getText("SURVEY_CANCEL_DIALOG_TITLE");
			var that = this;
			MessageBox.confirm(sCancelDialogText, {
				title: sCancelDialogTitle,
				onClose: function(oEvent) {
					if (oEvent && oEvent === "OK") {
						that._goBack();
						that._onRefreshFormContent();
					}
				}
			});
		},

		_goBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				this.getView().getModel("demo").setProperty("/ServiceMode", false);
				window.history.go(-1);
			} else {
				var oRouter = this.getRouter();
				oRouter.navTo("master", true);
			}
		},

		_onRefreshFormContent: function() {
			this.byId("idPageSurvey").destroyContent();
		},

		_onHandleErrorMsg: function(err) {
			var sMessage = "";
			var aErrorDetails = JSON.parse(err.responseText).error.innererror.errordetails;
			if (aErrorDetails.length > 0) {
				for (var i = 0; i < aErrorDetails.length; i++) {
					var oError = aErrorDetails[i];
					if (oError.severity === "error") {
						sMessage += (oError.message + "\n");
					}
				}
				if (!sMessage) {
					for (var i = 0; i < aErrorDetails.length; i++) {
						var oError = aErrorDetails[i];
						if (oError.severity === "info") {
							sMessage += (oError.message + "\n");
						}
					}
				}
			} else {
				sMessage = JSON.parse(err.responseText).error.message.value;
			}
			sap.m.MessageBox.error(sMessage, {
				title: this.getOwnerComponent().getModel("i18n").getProperty("ERROR_TITLE")
			});
		},

		_goToDetailPage: function(sGuid) {
			var oRouter = this.getRouter();
			oRouter.navTo("opportdisplay", {
				objectguid: sGuid
			});
		},

		onNavBacktoDisplay: function() {
			this._goBack();
			this._onRefreshFormContent();
		}

	});
});