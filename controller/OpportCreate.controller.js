sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"borusan/demo/model/models"
], function(Controller, JSONModel, History, Filter, FilterOperator, MessageBox, MessageToast, models) {
	"use strict";

	return Controller.extend("borusan.demo.controller.OpportCreate", {
		onInit: function() {
			var oView = this.getView();
			var oComp = this.getOwnerComponent();
			oView.addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);

			var oCreateModel = this.getOwnerComponent().getModel("opportCreate");
			if (!oCreateModel) {
				oCreateModel = models.createJSONModel();
				this.getOwnerComponent().setModel(oCreateModel, "opportCreate");
			}
		},

		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},

		_onRouteMatched: function(oEvent) {
			var sRouteName = oEvent.getParameter("name");
			if (sRouteName === "opportcreate" || sRouteName === "opportcopycreate") {
				this._resetCreatePageV2();
				this._getShValues(this);
				//var oCreateModel = this.getOwnerComponent().getModel("opportcreate");
				//oCreateModel.setProperty("/PostingDate", new Date());
				this._onGetAppoinmentTermin();
			}

			if (sRouteName === "opportcopycreate") {
				var oDetailModel = this.getView().getModel("opportDetail");
				if (oDetailModel) {
					this._copyDatasFromDetail(oDetailModel.getData());
				} else {
					this._goBack();
				}
			}

		},
		_onGetAppoinmentTermin: function() {
			var oDataModel = this.getView().getModel();
			var oCreate = this.getView().getModel("opportCreate");
			oDataModel.read("/AppointmentSet", {
				success: function(resp) {
					for (var i = 0; i < resp.results.length; i++) {
						delete resp.results[i].__metadata; // servisi patlatmamasi icin
					}
					oCreate.setProperty("/APPOINTMENT", resp.results);
				},
				error: function(err) {
					// TODO:  Error Mesaji Verilecek
				}
			});
		},
		onChangeDate: function(e) {
			var oCreate = this.getView().getModel("opportCreate");
			var oDate = e.getSource().getDateValue();
			var sDate = oDate.getFullYear() + "-" + (oDate.getMonth() + 1) + "-" + oDate.getDate() + "T00:00:00";
			var oParent = e.getSource().getParent();
			var sID = oParent.getId();
			var aID = sID.split("-");
			var sIndex = aID[aID.length - 1];
			oCreate.getProperty("/APPOINTMENT/" + sIndex).DateFrom = sDate;
		},
		
		onChangeTime : function (e) {
			
		},
		
		_getShValues: function(oController) {
			var oDataModel = oController.getOwnerComponent().getModel();
			var oCreateModel = oController.getOwnerComponent().getModel("opportCreate");

			oController._getStatusList(oDataModel, oCreateModel, "ZDEM");
			oController._getNoteTypeList(oDataModel, oCreateModel, "ZDEM");

			// oController._getTerminSet(oDataModel, oCreateModel);

			oController._getDomainValues(oDataModel, oCreateModel, "/TalepModelList", {
				FieldName: "TALEPEDILENMODE"

			});
			oController._getDomainValues(oDataModel, oCreateModel, "/YakitOdeyenList", {
				FieldName: "YAKITODEYEN"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/NakliyeGidisList", {
				FieldName: "NAKLIYE_GIDIS"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/NakliyeDonusList", {
				FieldName: "NAKLIYE_DONUS"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/AplikasyonList", {
				FieldName: "APLIKASYON"

			});
			oController._getDomainValues(oDataModel, oCreateModel, "/TedarikLokasyonList", {
				FieldName: "TEDARIKLOKASYON"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/TedarikSekliList", {
				FieldName: "TEDARIKSEKLI"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/TedarikKaynagiList", {
				FieldName: "TEDARIKKAYNAGI"

			});
			oController._getDomainValues(oDataModel, oCreateModel, "/DemoNedeniList", {
				FieldName: "DEMONEDENI"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/EgitimOperatoruList", {
				FieldName: "EGITIMOPERATORU"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/MakinaninDonYerList", {
				FieldName: "MAKINANINDONYER"

			});

			oController._getDomainValues(oDataModel, oCreateModel, "/MaliyetTypeList", {
				FieldName: "MALIYETTIP"
			});

			oController._getDomainValues(oDataModel, oCreateModel, "/MaliyetBirimList", {
				FieldName: "CURRENCY"
			});

		},

		_getStatusList: function(oDataModel, oJsonModel, sProcessType) {
			var aFilters = [];
			var oProcessTypeFilter = new Filter({
				path: 'ProcessType',
				operator: FilterOperator.EQ,
				value1: sProcessType
			});
			aFilters.push(oProcessTypeFilter);
			oDataModel.read("/CustomizingStatusListSet", {
				filters: aFilters,
				success: function(resp) {
					oJsonModel.setProperty("/StatusListSet", resp.results);
				},
				error: function(err) {

				}
			});
		},

		_getNoteTypeList: function(oDataModel, oJsonModel, sProcessType) {
			var aFilters = [];
			var oProcessTypeFilter = new Filter({
				path: 'ProcessType',
				operator: FilterOperator.EQ,
				value1: sProcessType
			});
			aFilters.push(oProcessTypeFilter);
			oDataModel.read("/CustomizingNoteCategorySHSet", {
				filters: aFilters,
				success: function(resp) {
					oJsonModel.setProperty("/NoteTypeList", resp.results);
				},
				error: function(err) {

				}
			});
		},
		_getDomainValues: function(oDataModel, oJsonModel, sPropertyName, mParameters) {
			var aFilters = [];
			if (mParameters.DomName) {
				var oDomainNameFilter = new Filter({
					path: "DomName",
					operator: FilterOperator.EQ,
					value1: mParameters.DomName
				});
				aFilters.push(oDomainNameFilter);
			}
			if (mParameters.FieldName) {
				var oFieldNameFilter = new Filter({
					path: "FieldName",
					operator: FilterOperator.EQ,
					value1: mParameters.FieldName
				});
				aFilters.push(oFieldNameFilter);
			}
			if (mParameters.ProcessType) {
				var oProcessTypeFilter = new Filter({
					path: "ProcessType",
					operator: FilterOperator.EQ,
					value1: mParameters.ProcessType
				});
				aFilters.push(oProcessTypeFilter);
			}
			oDataModel.read("/CustomizingDomainSHSet", {
				filters: aFilters,
				success: function(resp) {
					resp.results.unshift({});
					oJsonModel.setProperty(sPropertyName, resp.results);
				},
				error: function(err) {

				}
			});
		},

		_goBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = this.getRouter();
				oRouter.navTo("master", true);
			}
			this._resetCreatePageV2();
		},

		_resetCreatePageV2: function() {
			var oOpportCreateModel = this.getView().getModel("opportCreate");
			if (oOpportCreateModel) {
				oOpportCreateModel.setData({});
			}

			var aIdToReset = [
				"idDescription",
				"idPartnerFullname",
				"idMachModel",
				"idSerialNo",
				"idAuthoEngineerFullName",
				"idContactPersonName",
				"idRespPssrFullName",
				"idYakitOdeyen",
				"idYakitBilgi",
				"idCompReason",
				"idCompArriving",
				"idCompResp",
				"idPartnerno",
				"idMakinaOrder"
			];

			for (var i = 0; i < aIdToReset.length; i++) {
				var sId = aIdToReset[i];
				this._resetValueStateOfController(sId);
			}
		},

		_resetValueStateOfController: function(sID) {
			var oControl = this.byId(sID);
			if (oControl && oControl.setValueState) {
				oControl.setValueState(sap.ui.core.ValueState.None);
			}
		},

		onNavBack: function(oEvent) {
			this._goBack();
		},

		_copyDatasFromDetail: function(oDetailData) {
			var oCreateModel = this.getView().getModel("opportCreate");
			var oCreateData = oCreateModel.getData();
			oCreateData.Description = oDetailData.Description;
			oCreateData.PartnerNo = oDetailData.PartnerNo;
			oCreateData.PartnerFullname = oDetailData.PartnerFullname;
			oCreateData.Description = oDetailData.Description;
			oCreateData.Model = oDetailData.MachModel;
			oCreateData.SerialNo = oDetailData.SerialNo;

			oCreateData.RespPssr = oDetailData.RespPssr;
			oCreateData.RespPssrName = oDetailData.RespPssrName;

			oCreateData.RespPssr = oDetailData.RespPssr;
			oCreateData.RespPssrName = oDetailData.RespPssrName;

			oCreateData.ContactPerson = oDetailData.ContactPerson;
			oCreateData.ContactPersonName = oDetailData.ContactPersonName;

			oCreateData.EgitimOperatoru = oDetailData.EgitimOperatoru;

			oCreateModel.setData(oCreateData);
		},

		onFooterCancelBtnPress: function() {
			var oBundle = this.getView().getModel("i18n").getResourceBundle();
			var sCancelDialogText = oBundle.getText("CREATE_CANCEL_DIALOG_TEXT");
			var sCancelDialogTitle = oBundle.getText("CREATE_CANCEL_DIALOG_TITLE");
			var oController = this;
			MessageBox.confirm(sCancelDialogText, {
				title: sCancelDialogTitle,
				onClose: function(oEvent) {
					if (oEvent && oEvent === "OK") {
						oController._goBack();
					}
				}
			});
		},

		onCreate: function(oEvent) {
			var oCreateModel = this.getView().getModel("opportCreate");
			if (oCreateModel) {
				var oCreateModelData = oCreateModel.getData();
				var oData = {};
				oData.Mode = "A";
				oData.ProcessType = "ZDEM";
				oData.PartnerNo = oCreateModelData.PartnerNo;
				oData.Description = oCreateModelData.Description;

				oData.SerialNo = oCreateModelData.SerialNo;
				oData.YakitBilgi = oCreateModelData.YakitBilgi;
				oData.YakitOdeyen = oCreateModelData.YakitOdeyen;
				oData.MakinaOrder = oCreateModelData.MakinaOrder;
				oData.PostingDate = oCreateModelData.PostingDate;
				oData.RespPssr = oCreateModelData.RespPssr;
				oData.ContactPerson = oCreateModelData.ContactPerson;
				oData.ContactPerson = oCreateModelData.ContactPerson;
				oData.EgitimOperatoru = oCreateModelData.EgitimOperatoru;
				oData.DemoNedeni = oCreateModelData.DemoNedeni;
				oData.TalepEdilenMode = oCreateModelData.TalepEdilenMode;
				oData.NakliyeGidis = oCreateModelData.NakliyeGidis;
				oData.NakliyeDonus = oCreateModelData.NakliyeDonus;
				oData.Aplikasyon = oCreateModelData.Aplikasyon;
				oData.TedarikSekli = oCreateModelData.TedarikSekli;
				oData.TedarikKaynagi = oCreateModelData.TedarikKaynagi;
				oData.TedarikLokasyon = oCreateModelData.TedarikLokasyon;
				oData.PartnerAddrnumber = oCreateModelData.PartnerAddrnumber;

				if (oData.PostingDate) {
					// oData.Date.setMinutes(oData.Date.getUTCMinutes() - oData.Date.getTimezoneOffset());
					oData.PostingDate = new Date(oData.PostingDate.getTime() - (oData.PostingDate.getTimezoneOffset() * 60 * 1000));
				}

				oData.NOTES = [];
				if (oCreateModelData.NOTES) {
					jQuery.each(oCreateModelData.NOTES, function(key, el) {
						var oNote = {
							Content: el.Content,
							TextObjectId: el.TextObjectId,
							Mode: "A"
						};
						oData.NOTES.push(oNote);
					});
				}

				oData.APPOINTMENT = [];
				if (oCreateModelData.APPOINTMENT) {
					oData.APPOINTMENT = oCreateModelData.APPOINTMENT;
				}
				//var oI18nModel = this.getView().getModel("i18n");
				// var sMessage = this._validateDatas(oData, oI18nModel);
				// if (sMessage) {
				// MessageBox.show(sMessage, {
				// icon: "WARNING",
				// title: oI18nModel.getProperty("ERROR_TITLE")
				// });
				// } else {
				// this._callCreateService(oData, this);
				// }

				oData.MALIYET = [];
				if (oCreateModelData.MALIYET) {
					jQuery.each(oCreateModelData.MALIYET, function(key, el) {

						oData.MALIYET.push(el);
					});
				}

				this._callCreateService(oData, this);
			}
		},
		_validateElementV2: function(oData, sMessage, oParams) {
			var oI18nModel = this.getOwnerComponent().getModel("i18n");
			if (!oData[oParams.ElementKey]) {
				sMessage += oI18nModel.getProperty(oParams.FailModelText);
				sMessage += "\n";
				var oElement = this.byId(oParams.ElementId);
				if (oElement && oElement.setValueState) {
					oElement.setValueState(sap.ui.core.ValueState.Error);
				}
			}
			return sMessage;
		},
		_validateDatas: function(oData, oI18nModel) {
			var sMessage = "";

			var aElementParams = this._getElementParams();

			for (var i = 0; i < aElementParams.length; i++) {
				var oElementParam = aElementParams[i];
				sMessage = this._validateElementV2(oData, sMessage, oElementParam);
			}

			// EstiArrDate validation
			if (oData.CompResp === "1" && !oData.EstiArrDate) {
				sMessage += oI18nModel.getProperty("ESTI_AIR_DATE_VALIDATION_FAILED_TEXT");
				sMessage += "\n";
				var oEstAirDatePicker = this.byId("idEstiArrDate");
				if (oEstAirDatePicker && oEstAirDatePicker.setValueState) {
					oEstAirDatePicker.setValueState(sap.ui.core.ValueState.Error);
				}
			}
			// Note validation
			var aNotes = oData.NOTES;
			var sCompResp = oData.CompResp;
			sMessage = this._validateNotesDetail(aNotes, sCompResp, sMessage);

			return sMessage;
		},

		_getElementParams: function() {
			var aElementParams = [{
					ElementKey: "Description",
					FailModelText: "DESCRIPTION_VALIDATION_FAILED_TEXT",
					ElementId: "idDescription"
				}, {
					ElementKey: "PartnerNo",
					FailModelText: "PARTNER_VALIDATION_FAILED_TEXT",
					ElementId: "idPartnerFullname"
				}, {
					ElementKey: "Model",
					FailModelText: "MACHINE_MODEL_VALIDATION_FAILED_TEXT",
					ElementId: "idModel"
				}, {
					ElementKey: "SerialNo",
					FailModelText: "SERIAL_NO_VALIDATION_FAILED_TEXT",
					ElementId: "idSerialNo"
				},
				// {
				// ElementKey: "AuthoEngineer",
				// FailModelText: "RESPONSIBLE_ENGINEER_VALIDATION_FAILED_TEXT",
				// ElementId: "idAuthoEngineerFullName"
				// }, 
				{
					ElementKey: "RespPssr",
					FailModelText: "RESPONSIBLE_PSSR_VALIDATION_FAILED_TEXT",
					ElementId: "idRespPssrFullName"
				}, {
					ElementKey: "YakitBilgi",
					FailModelText: "YAKIT_BILGI_VALIDATION_FAILED_TEXT",
					ElementId: "idYakitBilgi"
				}, {
					ElementKey: "YakitOdeyen",
					FailModelText: "YAKIT_ODEYEN_VALIDATION_FAILED_TEXT",
					ElementId: "idYakitOdeyen"
				}, {
					ElementKey: "ContactPerson",
					FailModelText: "CONTACT_PERSON_VALIDATION_FAILED_TEXT",
					ElementId: "idContactPersonName"
				}, {
					ElementKey: "MakinaOrder",
					FailModelText: "MAKINA_ORDER_VALIDATION_FAILED_TEXT",
					ElementId: "idMakinaOrder"
				}, {
					ElementKey: "NakliyeGidis",
					FailModelText: "NAKLIYE_GIDIS_VALIDATION_FAILED_TEXT",
					ElementId: "idNakliyeGidis"
				}, {
					ElementKey: "NakliyeDonus",
					FailModelText: "NAKLIYE_DONUS_VALIDATION_FAILED_TEXT",
					ElementId: "idNakliyeDonus"
				}, {
					ElementKey: "Aplikasyon",
					FailModelText: "APLIKASYON_VALIDATION_FAILED_TEXT",
					ElementId: "idAplikasyon"
				}

			];

			return aElementParams;
		},

		_callCreateService: function(oData, oController) {
			var oDataModel = oController.getView().getModel();
			var oI18nModel = oController.getView().getModel("i18n");
			if (oDataModel && oI18nModel) {
				MessageToast.show(oI18nModel.getProperty("CREATE_BEGIN_TEXT"));
				oController.getView().setBusy(true);
				oDataModel.create("/DemoRequestSet", oData, {
					success: function(resp) {
						oController.getView().setBusy(false);
						oController._goToDetailPage(resp.Guid);

						var oMasterModel = oController.getOwnerComponent().getModel("Master");
						if (oMasterModel) {
							oMasterModel.setProperty("/DoUpdate", true);
						}
					},
					error: function(err) {
						oController.getView().setBusy(false);
						var sMessage = "";
						var aErrorDetails = JSON.parse(err.responseText).error.innererror.errordetails;
						if (aErrorDetails.length > 0) {
							for (var i = 0; i < aErrorDetails.length; i++) {
								var oError = aErrorDetails[i];
								if (oError.severity === "error") {
									sMessage += (oError.message + "\n");
								}
							}
							if (!sMessage) {
								for (var i = 0; i < aErrorDetails.length; i++) {
									var oError = aErrorDetails[i];
									if (oError.severity === "info") {
										sMessage += (oError.message + "\n");
									}
								}
							}
						} else {
							sMessage = JSON.parse(err.responseText).error.message.value;
						}
						sap.m.MessageBox.error(sMessage, {
							title: oController.getOwnerComponent().getModel("i18n").getProperty("ERROR_TITLE")
						});
					}
				});
			}
		},
		_goToDetailPage: function(sGuid) {
			var oRouter = this.getRouter();
			oRouter.navTo("opportdisplay", {
				objectguid: sGuid
			});
			this._resetCreatePageV2();
		},

		_getNoteDialog: function() {
			if (!this._noteDialogFragment) {
				this._noteDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.noteCreateDialog", this);
				this._noteDialogFragment.setModel(new JSONModel(), "noteModel");
				this.getView().addDependent(this._noteDialogFragment);
				this._noteDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			return this._noteDialogFragment;
		},
		_getPartnerDialog: function() {
			if (!this._partnerDialogFragment) {
				this._partnerDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.partnerSHDialogCreate", this);
				this._partnerDialogFragment.setModel(new JSONModel(), "partnerModel");
				this.getView().addDependent(this._partnerDialogFragment);
				this._partnerDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			return this._partnerDialogFragment;
		},

		onCustomerSH: function(oEvent) {
			this.getView().getModel("demo").setProperty("/FragmentType","account");
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setProperty("/RolType", "CRM000");
			oPartnerModel.setProperty("/PARTNER_DIALOG_TITLE", "M\u00FC\u015Fteri");
			oPartnerModel.setProperty("/PropertyPathToSet1", "/PartnerFullname");
			oPartnerModel.setProperty("/PropertyPathToSet2", "/PartnerNo");
			var oBinding = oPartnerDialog.getBinding("items");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: 'RolType',
				operator: FilterOperator.EQ,
				value1: "CRM000"
			});
			aFilters.push(oQueryFilter);
			// oBinding = oPartnerDialog.getContent()[1].getBinding("items")
			oBinding.filter(aFilters);
			oPartnerDialog.open();
		},
		onRespPssrSH: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setProperty("/RolType", "BUP003");
			oPartnerModel.setProperty("/PARTNER_DIALOG_TITLE", "Sorumlu ");
			oPartnerModel.setProperty("/PropertyPathToSet1", "/RespPssrName");
			oPartnerModel.setProperty("/PropertyPathToSet2", "/RespPssr");
			var oBinding = oPartnerDialog.getBinding("items");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: "BUP003"
			});
			aFilters.push(oQueryFilter);
			oBinding.filter(aFilters);
			oPartnerDialog.open();
		},

		onContactPersonSH: function(oEvent) {
			this.getView().getModel("demo").setProperty("/FragmentType","contact");
			var oPartnerDialog = this._getPartnerDialog();
			var oCreate = this.getView().getModel("opportCreate");
			var smusteri = oCreate.getProperty("/PartnerNo");

			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setProperty("/RolType", "BUP001");
			oPartnerModel.setProperty("/PARTNER_DIALOG_TITLE", "\u0130lgili ki\u015Filer");
			oPartnerModel.setProperty("/PropertyPathToSet1", "/ContactPersonName");
			oPartnerModel.setProperty("/PropertyPathToSet2", "/ContactPerson");
			var oBinding = oPartnerDialog.getBinding("items");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: "BUP001"
			});
			aFilters.push(oQueryFilter);

			var oMusterifilter = new Filter({
				path: "CustomerNo",
				operator: FilterOperator.EQ,
				value1: smusteri
			});
			aFilters.push(oMusterifilter);
			oBinding.filter(aFilters);
			oPartnerDialog.open();
		},

		onAuthoEngineerSH: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setProperty("/RolType", "BUP003");
			oPartnerModel.setProperty("/PARTNER_DIALOG_TITLE", "");
			oPartnerModel.setProperty("/PropertyPathToSet1", "/AuthoEngineerName");
			oPartnerModel.setProperty("/PropertyPathToSet2", "/AuthoEngineer");
			var oBinding = oPartnerDialog.getBinding("items");
			var aFilters = [];
			var oQueryFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: "BUP003"
			});
			aFilters.push(oQueryFilter);
			oBinding.filter(aFilters);
			oPartnerDialog.open();
		},
		onPartnerSearch: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			var sRoleType = oPartnerModel.getProperty("/RolType");
			var sQuery = oEvent.getParameter("value");
			var oBinding = oEvent.getSource().getBinding("items");
			var oSource = oEvent.getSource();
			if (sRoleType === "CRM000") {
				sQuery = sQuery.toLocaleUpperCase();
			} else {
				sQuery = sQuery.toLocaleLowerCase();
			}
			if (oSource._searchField) {
				var oSearchField = oSource._searchField;
				oSearchField.setValue(sQuery);
			}
			var aFilters = [];
			var oRolTypeFilter = new Filter({
				path: "RolType",
				operator: FilterOperator.EQ,
				value1: sRoleType
			});
			aFilters.push(oRolTypeFilter);
			if (sQuery) {
				var oQueryFilter = new Filter({
					path: "PartnerFullname",
					operator: FilterOperator.EQ,
					value1: sQuery
				});
				aFilters.push(oQueryFilter);
			}
			oBinding.filter(aFilters);
		},

		onPartnerDialogConfirm: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			var aSelectedItems = oEvent.getParameter("selectedItems");
			if (aSelectedItems && aSelectedItems.length > 0) {
				var oSelectedItem = aSelectedItems[0];
				var oBindingContext = oSelectedItem.getBindingContext();
				var oModel = oBindingContext.getModel();
				var sPath = oBindingContext.getPath();
				var oSelectedData = oModel.getProperty(sPath);
				var oCreateModel = this.getOwnerComponent().getModel("opportCreate");
				oCreateModel.setProperty(oPartnerModel.getProperty("/PropertyPathToSet1"), oSelectedData.PartnerFullname);
				oCreateModel.setProperty(oPartnerModel.getProperty("/PropertyPathToSet2"), oSelectedData.PartnerNo);
				if (oPartnerModel.getProperty("/PropertyPathToSet2") === "/PartnerNo" && oSelectedData.PartnerNo) {
					this._validatePartnerInputs("idPartnerno");
				}
				if (oPartnerModel.getProperty("/PropertyPathToSet2") === "/AuthoEngineer" && oSelectedData.PartnerNo) {
					this._validatePartnerInputs("idAuthoEngineerFullName");
				}
				if (oPartnerModel.getProperty("/PropertyPathToSet2") === "/RespPssr" && oSelectedData.PartnerNo) {
					this._validatePartnerInputs("idRespPssrFullName");
				}

				if (oPartnerModel.getProperty("/PropertyPathToSet2") === "/RespPssr" && oSelectedData.PartnerNo) {
					this._validatePartnerInputs("idContactPersonName");
				}
				oPartnerModel.setData({});
			}
			this._onSelectAccountAddress(oSelectedData.PartnerNo);
		},
		_onSelectAccountAddress: function(sPartnerNo) {
			var that = this;
			var oModel = this.getView().getModel();
			var oCreate = this.getView().getModel("opportCreate");
			var oFilter = new sap.ui.model.Filter({
				path: "accountID",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: sPartnerNo
			});
			oModel.read("/BPAddressSet", {
				filters: [oFilter],
				success: function(resp) {
					oCreate.setProperty("/AddressCollection", resp.results);
					if (!that.AccountAddressDialog) {
						that.AccountAddressDialog = sap.ui.xmlfragment("borusan.demo.fragments.AccountAddress", that);
						that.AccountAddressDialog.setModel(that.getView().getModel("i18n"), "i18n");
						that.getView().addDependent(that.AccountAddressDialog);
					}
					that.AccountAddressDialog.open();
				},
				errorr: function(err) {
					// TODO: Error Mesaji basilacak
				}
			});
		},
		onAddressSearch: function(e) {
			var sValue = e.getParameter("value");
			var oAddress = new sap.ui.model.Filter("address", sap.ui.model.FilterOperator.Contains, sValue);
			e.getSource().getBinding("items").filter(oAddress);
		},
		onAddressConfirm: function(e) {
			var oDemo = this.getView().getModel("demo");
			var sFragment = oDemo.getProperty("/FragmentType");
			var oCreate = this.getView().getModel("opportCreate");
			var aContexts = e.getParameter("selectedContexts");
			var oI18n = this.getView().getModel("i18n");
			if (aContexts && aContexts.length) {
				var sAddressNumber = aContexts.map(function(oContext) {
					return oContext.getObject().addressNumber;
				}).join(", ");
				if (sFragment && sFragment === "account") {
					oCreate.setProperty("/PartnerAddrnumber", sAddressNumber);
				} else {
					oCreate.setProperty("/ContactPersonAddrnumber", sAddressNumber);
				}
				var sSuccess = oI18n.getProperty("CHOSEN_SUCCESS");
				MessageToast.show(sAddressNumber + " " +sSuccess);
			} else {
				var sWarning = oI18n.getProperty("CHOSEN_WARNING");
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.warning(
					sWarning,
					{
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
		
			}
			e.getSource().getBinding("items").filter([]);
		},
		onNoteAddBtnPress: function(oEvent) {
			var oI18nModel = this.getView().getModel("i18n");
			var oNoteDialog = this._getNoteDialog();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			oNoteModel.setProperty("/textSuccessButton", oI18nModel.getProperty("ADD"));
			oNoteModel.setProperty("/NoteDialogTitleText", oI18nModel.getProperty("NOTE_DIALOG_TITLE_CREATE"));

			var oCreateModel = this.getOwnerComponent().getModel("opportCreate");
			var aNoteTypeList = oCreateModel.getProperty("/NoteTypeList");
			var aSelectableNoteTypeList = [];
			var aNotes = oCreateModel.getProperty("/NOTES");
			for (var i = 0; i < aNoteTypeList.length; i++) {
				var bControl = true;
				if (aNotes && aNotes.length > 0) {
					for (var j = 0; j < aNotes.length; j++) {
						if (aNoteTypeList[i].Textid === aNotes[j].TextObjectId) {
							bControl = false;
						}
					}
				}
				if (bControl) {
					aSelectableNoteTypeList.push(aNoteTypeList[i]);
				}
			}
			oNoteModel.setProperty("/NoteTypeList", aSelectableNoteTypeList);
			oNoteModel.setProperty("/NoteTypeSelectEnabled", true);
			if (aSelectableNoteTypeList.length > 0) {
				oNoteDialog.open();
			} else {
				//var sMessage = this.getView().getModel("i18n").getProperty("MAX_LIMIT_OF_NOTES_EXCEEDED_TEXT");
				//MessageToast.show(sMessage);
				oNoteModel.setData({});
			}
		},

		onPartnerDialogCancel: function(oEvent) {
			var oPartnerDialog = this._getPartnerDialog();
			var oPartnerModel = oPartnerDialog.getModel("partnerModel");
			oPartnerModel.setData({});
		},
		onNoteDialogCancel: function(oEvent) {
			var oNoteDialog = this._getNoteDialog();
			oNoteDialog.close();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			oNoteModel.setData({});
		},

		onNoteUpdateBtnPress: function(oEvent) {
			var oI18nModel = this.getView().getModel("i18n");
			var oNoteDialog = this._getNoteDialog();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			var oCreateModel = this.getOwnerComponent().getModel("opportCreate");
			var oBindingContext = oEvent.getSource().getBindingContext("opportCreate");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);
			oNoteModel.setProperty("/NoteDialogTitleText", oI18nModel.getProperty("NOTE_DIALOG_TITLE_EDIT"));
			oNoteModel.setProperty("/NoteType", oSelectedNote.TextObjectId);
			oNoteModel.setProperty("/NoteTypeList", oCreateModel.getProperty("/NoteTypeList"));
			oNoteModel.setProperty("/NoteTextDialog", oSelectedNote.Content);
			oNoteModel.setProperty("/IsUpdate", true);
			oNoteModel.setProperty("/SPath", sPath);
			oNoteModel.setProperty("/NoteTypeSelectEnabled", false);
			oNoteDialog.open();

		},
		onNoteDeleteBtnPress: function(oEvent) {
			var oCreateModel = this.getView().getModel("opportCreate");
			var oBindingContext = oEvent.getSource().getBindingContext("opportCreate");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);
			var oI18nModel = this.getView().getModel("i18n");
			var oResourceBundle = oI18nModel.getResourceBundle();
			var oDeleteDialogText = oResourceBundle.getText("DELETE_DIALOG_TEXT", [oSelectedNote.NoteType]);
			var oDeleteDialogTitle = oResourceBundle.getText("DELETE_DIALOG_TITLE");
			MessageBox.show(oDeleteDialogText, {
				icon: "WARNING",
				title: oDeleteDialogTitle,
				actions: ["OK", "CANCEL"],
				onClose: function(oCloseEvent) {
					if (oCloseEvent && oCloseEvent === "OK" && sPath) {
						var aNotes = oCreateModel.getProperty("/NOTES");
						var iSelectedNoteIndex = aNotes.indexOf(oSelectedNote);
						if (iSelectedNoteIndex !== -1) {
							aNotes.splice(iSelectedNoteIndex, 1);
							oCreateModel.setProperty("/NOTES", aNotes);
						}
					}
					oCreateModel.setProperty("/SelectedNotePath", "");
				}
			});
		},
		onNoteDialogSave: function(oEvent) {
			var oNoteDialog = this._getNoteDialog();
			oNoteDialog.close();
			var oNoteModel = oNoteDialog.getModel("noteModel");
			var oCreateModel = oNoteDialog.getModel("opportCreate");
			var oNoteData = oNoteModel.getData();
			if (!oNoteData.NoteTextDialog) {
				MessageToast.show(this.getOwnerComponent().getModel("i18n").getProperty("EMPTY_NOTE_TOAST_TEXT"));
			} else {
				var oNote;
				if (oNoteData.IsUpdate && oNoteData.SPath) {
					oNote = oCreateModel.getProperty(oNoteData.SPath);
					oNote.NoteType = oEvent.getSource().getParent().getAggregation("content")[0].getContent()[1].getSelectedItem().getText();
					oNote.Content = oNoteData.NoteTextDialog;
					oNote.TextObjectId = oNoteData.NoteType;
					oCreateModel.setProperty(oNoteData.SPath, oNote);
				} else {
					oNote = {};
					oNote.NoteType = oEvent.getSource().getParent().getAggregation("content")[0].getContent()[1].getSelectedItem().getText();
					oNote.Mode = "A";
					oNote.Content = oNoteData.NoteTextDialog;
					oNote.TextObjectId = oNoteData.NoteType;
					var aNotes = oCreateModel.getProperty("/NOTES");
					if (!aNotes) {
						aNotes = [];
					}
					aNotes.push(oNote);
					aNotes = oCreateModel.setProperty("/NOTES", aNotes);
				}
				oNoteModel.setData({});
			}
		},
		onDescriptionLiveChange: function(oEvent) {
			this._validateInput(oEvent);
		},
		onModelLiveChange: function(oEvent) {
			this._validateSelect(oEvent);
		},
		onSerialNoLiveChange: function(oEvent) {
			this._validateInput(oEvent);
		},

		onYakitBilgiLiveChange: function(oEvent) {
			this._validateSelect(oEvent);
		},

		onNakliyeGidisLiveChange: function(oEvent) {
			this._validateSelect(oEvent);
		},
		onNakliyeDonusLiveChange: function(oEvent) {
			this._validateSelect(oEvent);
		},

		onTalepEdilenModeLiveChange: function(oEvent) {
			this._validateSelect(oEvent);
		},

		_validateSelect: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			var oSource = oEvent.getSource();
			var oSelectedText = oSelectedItem.getText();
			if (oSelectedText && oSource.setValueState) {
				oSource.setValueState(sap.ui.core.ValueState.None);
			}
		},

		_validatePartnerInputs: function(sId) {
			var oInput = this.byId(sId);
			var sValue = oInput.getValue();
			var oCurrentValueState = oInput.getValueState();
			if (oInput.getRequired && oInput.getRequired() && oInput.setValueState && sValue && oCurrentValueState === sap.ui.core.ValueState
				.Error) {
				oInput.setValueState(sap.ui.core.ValueState.None);
			}
		},

		onValidationError: function(oEvent) {
			var oSource = oEvent.getSource();
			var sMessage = oEvent.getParameter("message");
			if (oSource.setValueState) {
				oSource.setValueState(sap.ui.core.ValueState.Error);
				oSource.setValueStateText(sMessage);
			}
		},

		onValidationSuccess: function(oEvent) {
			var oSource = oEvent.getSource();
			if (oSource.setValueState) {
				oSource.setValueState(sap.ui.core.ValueState.None);
			}
		},

		onNotifPartner1LC: function(oEvent) {
			var oCreateModel = this.getView().getModel("opportCreate");
			oCreateModel.setProperty("/NotifPartner1", "");
			// delete oCreateData.NotifPartner1;
			// oCreateModel.setData(oCreateData);

		},

		onNotifPartner2LC: function(oEvent) {
			var oCreateModel = this.getView().getModel("opportCreate");
			oCreateModel.setProperty("/NotifPartner2", "");
		},

		onMarginAddBtnPress: function(oEvent) {
			this.getView().setBusy(true);
			var oI18nModel = this.getView().getModel("i18n");
			var oDataModel = this.getView().getModel();
			var oCreateModel = this.getOwnerComponent().getModel("opportCreate");
			var oMargin = this.getView().getModel("demo");

			oMargin.setProperty("/MaliyetTip", "");
			oMargin.setProperty("/BeklenenMaliyet", "");
			oMargin.setProperty("/BeklenenMaliyetBirim", "");
			oMargin.setProperty("/GerceklesenMaliyet", "");
			oMargin.setProperty("/GerceklesenMaliyetBirim", "");
			oMargin.setProperty("/MaliyetTypeList", oCreateModel.getProperty("/MaliyetTypeList"));

			oMargin.setProperty("/MaliyetBirimList", oCreateModel.getProperty("/MaliyetBirimList"));

			var oMarginDialog = this._getMarginDialog();
			oMarginDialog.open();

		},
		_getMarginDialog: function() {

			if (!this._marginDialogFragment) {
				this._marginDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.maliyetCreateDialog", this);
				this.getView().addDependent(this._marginDialogFragment);
				this._marginDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			// this._marginDialogFragment.open();
			return this._marginDialogFragment;
		},
		onMarginDialogCancel: function() {
			if (this._marginDialogFragment) {
				this._marginDialogFragment.close();
			}
			this.getView().setBusy(false);

		},
		onMarginDialogSave: function(oEvent) {
			var oMargin = this.getView().getModel("demo");
			var oCreateModel = this.getView().getModel("opportCreate");
			var oMarginData = oMargin.getData();

			if (!oMarginData.MaliyetTip) {
				MessageToast.show(this.getOwnerComponent().getModel("i18n").getProperty("EMPTY_MALIYET_TYPE_TEXT"));
			} else {
				var oMarginNew;
				if (oMarginData.IsUpdate && oMarginData.SPath) {
					oMarginNew = oCreateModel.getProperty(oMarginData.SPath);
					oMarginNew.MaliyetTip = oMarginData.MaliyetTip;
					oCreateModel.setProperty(oMarginData.SPath, oMarginNew);
				} else {
					oMarginNew = {};
					// oMarginNew.Mode = "A";
					oMarginNew.MaliyetTip = oMarginData.MaliyetTip;
					oMarginNew.BeklenenMaliyet = oMarginData.BeklenenMaliyet;
					oMarginNew.BeklenenMaliyetBirim = oMarginData.BeklenenMaliyetBirim;
					oMarginNew.GerceklesenMaliyet = oMarginData.GerceklesenMaliyet;
					oMarginNew.GerceklesenMaliyetBirim = oMarginData.GerceklesenMaliyetBirim;

					var aMargin = oCreateModel.getProperty("/MALIYET");
					if (!aMargin) {
						aMargin = [];
					}
					aMargin.push(oMarginNew);
					aMargin = oCreateModel.setProperty("/MALIYET", aMargin);
				}
				oMargin.setData({});
			}
			if (this._marginDialogFragment) {
				this._marginDialogFragment.close();
				this.getView().setBusy(false);
			}

		},

		onMarginUpdateBtnPress: function(oEvent) {
			var oI18nModel = this.getView().getModel("i18n");
			var oCreateModel = this.getOwnerComponent().getModel("opportCreate");
			var oMarginDialog = this._getMarginDialog();
			var oMargin = oMarginDialog.getModel("demo");
			var oBindingContext = oEvent.getSource().getBindingContext("opportCreate");
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedNote = oModel.getProperty(sPath);

			oMargin.setProperty("/MaliyetTip", oSelectedNote.MaliyetTip);
			oMargin.setProperty("/BeklenenMaliyet", oSelectedNote.BeklenenMaliyet);
			oMargin.setProperty("/BeklenenMaliyetBirim", oSelectedNote.BeklenenMaliyetBirim);
			oMargin.setProperty("/GerceklesenMaliyet", oSelectedNote.GerceklesenMaliyet);
			oMargin.setProperty("/GerceklesenMaliyetBirim", oSelectedNote.GerceklesenMaliyetBirim);
			oMargin.setProperty("/MaliyetTypeList", oCreateModel.getProperty("/MaliyetTypeList"));
			oMargin.setProperty("/MaliyetBirimList", oCreateModel.getProperty("/MaliyetBirimList"));

			oMargin.setProperty("/IsUpdate", true);
			oMargin.setProperty("/SPath", sPath);
			oMargin.setProperty("/NoteTypeSelectEnabled", false);

			oMarginDialog.open();
		}

	});

});