sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/Device"
], function(Controller,Device) {
	"use strict";

	return Controller.extend("borusan.demo.controller.Result", {


		onInit: function() {

			var oComp = this.getOwnerComponent();
			this.getView().addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);
			this.aSurveyCollection = [];
		},

		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},
		_onRouteMatched: function(e) {
			var sRouteName = e.getParameter("name");
			if (sRouteName === "result") {
				if (sap.ui.Device.system.phone) {
					this.getView().getModel("demo").setProperty("/PageNavButton",true);
				} else {
					this.getView().getModel("demo").setProperty("/PageNavButton",false);
				}
				var sObjectID = e.getParameter("arguments").objectid;
				var oDemo = this.getView().getModel("demo");
				var oI18n = this.getView().getModel("i18n");
				var sMessage = sObjectID + " Nolu " + oI18n.getProperty("RESULT_SUCCESS");
				oDemo.setProperty("/ResultMsg",sMessage);
				if (oDemo.getProperty("/ResultError")) {
					var sErrorMsg = sObjectID + " Nolu " + oI18n.getProperty("RESULT_ERROR");
					oDemo.setProperty("/ResultMsg",sErrorMsg);	
				}
			}
		},
		
		onNavBackMaster : function () {
			var oRouter = this.getRouter();
			oRouter.navTo("master", true);
		}
		
	});
});