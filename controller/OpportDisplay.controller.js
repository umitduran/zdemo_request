sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"borusan/demo/model/models",
	"borusan/demo/model/formatter"
], function(Controller, JSONModel, History, Filter, FilterOperator, MessageBox, MessageToast, models, formatter) {
	"use strict";

	return Controller.extend("borusan.demo.controller.OpportDisplay", {
		formatter: formatter,
		onInit: function() {

			var oView = this.getView();

			var oComp = this.getOwnerComponent();

			oView.addStyleClass(oComp.getContentDensityClass());

			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);

			var oDetailModel = this.getOwnerComponent().getModel("opportDisplay");
			if (!oDetailModel) {
				var oOpportDetailModel = models.createJSONModel();
				this.getOwnerComponent().setModel(oOpportDetailModel, "opportDisplay");
			}
			if (sap.ui.Device.system.phone) {
				var oOpportDisplayPage = this.getView().byId("idOpportDisplayPage");
				oOpportDisplayPage.setShowNavButton(true);
			}
		},

		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},

		_onRouteMatched: function(oEvent) {
			var sRouteName = oEvent.getParameter("name");
			if (sRouteName === "opportdisplay") {
				var sObjectGuid = oEvent.getParameter("arguments").objectguid;
				var bGetService = this.getView().getModel("demo").getProperty("/ServiceMode");
				if (bGetService === false) {
					// Sayfa yenilemeye gerek yok 
				} else {
					this._loadServiceDetailPage(this, sObjectGuid);
				}

			}
		},

		_loadServiceDetailPage: function(oController, sGuid) {
			// oController._hideAllFooterButtons();
			var that = this;
			var sPath = "/DemoRequestSet(guid'" + sGuid + "')";
			var oDataModel = oController.getView().getModel();
			var oDetailModel = oController.getOwnerComponent().getModel("opportDisplay");
			oController.getView().setBusy(true);
			oDataModel.read(sPath, {
				//Expand eklenecek
				urlParameters: {
					"$expand": "NOTES,APPOINTMENT,ATTENDEES,MALIYET,SURVEYS,ATTACHMENT"
				},
				success: function(resp) {
					oController.getView().setBusy(false);
					console.log(resp.Stat); // Test icin koydum
					if (!resp.ObjectId) {
						oController._goToMainPage();
					} else {
						oDetailModel.setData(resp);
						//oController._onChangeButtonVisible(resp.Stat,resp.CanBeEditable);
						oController._onSetIconCount(resp);
					}
				},
				error: function(err) {
					oController.getView().setBusy(false);
					that._onHandleErrorMsg(err);
				}
			});
		},

		_goToMainPage: function() {
			var oRouter = this.getRouter();
			oRouter.navTo("master", true);
		},

		onNavigateToEditPage: function() {
			var oDisplay = this.getView().getModel("opportDisplay");
			var sGuid = oDisplay.getProperty("/Guid");
			var oRouter = this.getRouter();
			oRouter.navTo("opportedit", {
				objectguid: sGuid
			});
		},

		onIsEmriDemoStatu: function() {
			this.getView().setBusy(true);
			var oDisplay = this.getView().getModel("opportDisplay");
			var oDemo = this.getView().getModel("demo");
			var sGuid = oDisplay.getProperty("/Guid");
			//var sStatu = oDisplay.getProperty("/Stat");
			var sCanEdit = oDisplay.getProperty("/CanBeEditable");
			oDemo.setProperty("/ResultSuccess",true);
			oDemo.setProperty("/ResultError",false);
			this._onUpdateDemoStatu("E0036", sGuid);
			oDemo.setProperty("/CanBeEditable",sCanEdit);
		},

		onIadeDemoStatu: function() {
			this.getView().setBusy(true);
			var oDisplay = this.getView().getModel("opportDisplay");
			var oDemo = this.getView().getModel("demo");
			var sGuid = oDisplay.getProperty("/Guid");
			//var sStatu = oDisplay.getProperty("/Stat");
			var sCanEdit = oDisplay.getProperty("/CanBeEditable");
			oDemo.setProperty("/ResultSuccess",true);
			oDemo.setProperty("/ResultError",false);
			this._onUpdateDemoStatu("E0037", sGuid);
			oDemo.setProperty("/CanBeEditable", sCanEdit);
		},

		onConfirmDemoStatu: function() {
			this.getView().setBusy(true);
			var oDisplay = this.getView().getModel("opportDisplay");
			var oDemo = this.getView().getModel("demo");
			var sGuid = oDisplay.getProperty("/Guid");
			var sStatu = oDisplay.getProperty("/Stat");
			var sCanEdit = oDisplay.getProperty("/CanBeEditable");
			oDemo.setProperty("/ResultSuccess",true);
			oDemo.setProperty("/ResultError",false);
			if (sStatu === "E0022") {
				this._onUpdateDemoStatu("E0023", sGuid);
			} else
			if (sStatu === "E0033") {
				this._onUpdateDemoStatu("E0034", sGuid);
			} else
			if (sStatu === "E0029") {
				this._onUpdateDemoStatu("E0031", sGuid);
			}
			if (sStatu === "E0031") {
				this._onUpdateDemoStatu("E0032", sGuid);
			}
			oDemo.setProperty("/CanBeEditable", sCanEdit);
		},
		onRejectDemoStatu: function() {
			this.getView().setBusy(true);
			var oDataModel = this.getView().getModel();
			var oDisplay = this.getView().getModel("opportDisplay");
			var oDemo = this.getView().getModel("demo");
			var sStatu = oDisplay.getProperty("/Stat");
			var sCanEdit = oDisplay.getProperty("/CanBeEditable");
			//var sGuid = oDisplay.getProperty("/Guid");
			var sNextStatus = sStatu;
			if (sStatu === "E0022") {
				sNextStatus = "E0024";
			} else
			if (sStatu === "E0033") {
				sNextStatus = "E0035";
			}
			oDemo.setProperty("/NextStatus", sNextStatus);
			oDemo.setProperty("/CanBeEditable", sCanEdit);
			oDemo.setProperty("/ResultSuccess",false);
			oDemo.setProperty("/ResultError",true);

			this._getDomainValues(oDataModel, oDemo, "/ReasonTypeList", {
				FieldName: "REASON",
				Status: sNextStatus
			});

			// if (sNextStatus != sStatu) {
			// 	this._onUpdateDemoStatu(sNextStatus, sGuid);
			// }

		},

		onReasonDialogCancel: function() {
			if (this._reasonDialogFragment) {
				this._reasonDialogFragment.close();
			}
			this.getView().setBusy(false);
		},

		onReasonDialogSave: function() {
			var oDemo = this.getView().getModel("demo");
			var oDisplay = this.getView().getModel("opportDisplay");
			var sGuid = oDisplay.getProperty("/Guid");
			var sNextStatus = oDemo.getProperty("/NextStatus");
			this.bReject = true;
			this._onUpdateDemoStatu(sNextStatus, sGuid);
			if (this._reasonDialogFragment) {
				this._reasonDialogFragment.close();
			}
		},

		_onUpdateDemoStatu: function(sStatu, sGuid) {
			var that = this;
			var oModel = this.getView().getModel();
			var oDemo = this.getView().getModel("demo");
			var oDisplay = this.getView().getModel("opportDisplay");
			var oI18n = this.getView().getModel("i18n");
			var oUpdate = {};
			oUpdate.Stat = sStatu;
			//var sCanBeEditable = oDemo.getProperty("/CanBeEditable");
			if (this.bReject) {
				oUpdate.Reason = oDemo.getProperty("/Reason");
				oUpdate.TextContent = oDemo.getProperty("/NoteTextDialog");
				this.bReject = false;
			}
			var sPath = "/DemoRequestSet(guid'" + sGuid + "')";
			oModel.update(sPath, oUpdate, {
				success: function(resp) {
					//MessageToast.show(oI18n.getProperty("UPDATE_SUCCESS_TEXT"));
					//that._onChangeButtonVisible(sStatu,sCanBeEditable);
					var oRouter = that.getRouter();
					oRouter.navTo("result", {
						objectid: oDisplay.getProperty("/ObjectId")
					});
					that.getView().setBusy(false);
				},
				error: function(err) {
					that.getView().setBusy(false);
					var sMessage = "";
					var aErrorDetails = JSON.parse(err.responseText).error.innererror.errordetails;
					if (aErrorDetails.length > 0) {
						for (var i = 0; i < aErrorDetails.length; i++) {
							var oError = aErrorDetails[i];
							if (oError.severity === "error") {
								sMessage += (oError.message + "\n");
							}
						}
						if (!sMessage) {
							for (var i = 0; i < aErrorDetails.length; i++) {
								oError = aErrorDetails[i];
								if (oError.severity === "info") {
									sMessage += (oError.message + "\n");
								}
							}
						}
					} else {
						sMessage = JSON.parse(err.responseText).error.message.value;
					}
					sap.m.MessageBox.error(sMessage, {
						title: that.getOwnerComponent().getModel("i18n").getProperty("ERROR_TITLE")
						
					});
				}
			});
		},

		_getReasonDialog: function() {
			var oDemo = this.getView().getModel("demo");
			oDemo.setProperty("/NoteTextDialog", "");
			oDemo.setProperty("/Reason", "");
			if (!this._reasonDialogFragment) {
				this._reasonDialogFragment = sap.ui.xmlfragment("borusan.demo.fragments.reasonDialog", this);
				this.getView().addDependent(this._reasonDialogFragment);
				this._reasonDialogFragment.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			}
			this._reasonDialogFragment.open();
		},

		_getDomainValues: function(oDataModel, oJsonModel, sPropertyName, mParameters) {
			var that = this;
			var aFilter = [];
			var oFieldName = new sap.ui.model.Filter({
				path: "FieldName",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: mParameters.FieldName
			});
			aFilter.push(oFieldName);
			var oStatus = new sap.ui.model.Filter({
				path: "Status",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: mParameters.Status
			});
			aFilter.push(oStatus);

			oDataModel.read("/CustomizingDomainSHSet", {
				filters: aFilter,
				success: function(resp) {
					resp.results.unshift({});
					oJsonModel.setProperty(sPropertyName, resp.results);
					if (mParameters.FieldName === "REASON") {
						that._getReasonDialog();
					}
				},
				error: function(err) {
					// TODO: Error Mesaji verilecek
				}
			});
		},

		_onChangeButtonVisible: function(sStatu, sEditable) {
			if ((sStatu === "E0022" || sStatu === "E0033") && (sEditable && sEditable === "X")) {
				this.byId("idOnayButton").setVisible(true);
				this.byId("idReddetButton").setVisible(true);
			} else {
				this.byId("idOnayButton").setVisible(false);
				this.byId("idReddetButton").setVisible(false);
			}
			if ((sStatu === "E0029") && (sEditable && sEditable === "X")) {
				this.byId("idDemoButton").setVisible(true);
				this.byId("idOnayButton").setVisible(false);
				this.byId("idReddetButton").setVisible(false);
			} else {
				this.byId("idDemoButton").setVisible(false);
			}
		},

		onNavigateToSurvey: function(e) {
			// TODO: Secilen anketin ID sini gonderecegiz gecisi olarak index aldim !!!!!!!!!!!!!1
			var sID = e.getSource().getParent().getId();
			var sIndex = sID.split("-")[2];
			var oRouter = this.getRouter();
			oRouter.navTo("survey", {
				index: sIndex
			});

		},

		_onSetIconCount: function(oResp) {
			var oDemo = this.getView().getModel("demo");
			oDemo.setProperty("/NotesCount", oResp.NOTES.results.length.toString());
			oDemo.setProperty("/AppoinmentCount", oResp.APPOINTMENT.results.length.toString());
			oDemo.setProperty("/AttendeesCount", oResp.ATTENDEES.results.length.toString());
			oDemo.setProperty("/MaliyetCount", oResp.MALIYET.results.length.toString());
			oDemo.setProperty("/SurveysCount", oResp.SURVEYS.results.length.toString());
			oDemo.setProperty("/AttachmentCount", oResp.ATTACHMENT.results.length.toString());
		},

		_onHandleErrorMsg: function(err) {
			var sMessage = "";
			var aErrorDetails = JSON.parse(err.responseText).error.innererror.errordetails;
			if (aErrorDetails.length > 0) {
				for (var i = 0; i < aErrorDetails.length; i++) {
					var oError = aErrorDetails[i];
					if (oError.severity === "error") {
						sMessage += (oError.message + "\n");
					}
				}
				if (!sMessage) {
					for (var i = 0; i < aErrorDetails.length; i++) {
						var oError = aErrorDetails[i];
						if (oError.severity === "info") {
							sMessage += (oError.message + "\n");
						}
					}
				}
			} else {
				sMessage = JSON.parse(err.responseText).error.message.value;
			}
			sap.m.MessageBox.error(sMessage, {
				title: this.getOwnerComponent().getModel("i18n").getProperty("ERROR_TITLE")
			});
		},

		// gulsah
		onAttachmentChange: function(e) {
			var m = this.getView().getModel();
			var u = e.getSource();
			u.setUploadUrl("/sap/opu/odata/sap/ZCRM_DEMO_REQUEST_SRV/DemoRequestSet(guid'" + this.getOwnerComponent().getModel(
				"opportDisplay").getProperty("/Guid") + "')/ATTACHMENT");
			var f = e.getParameter("mParameters").files[0];
			var t = m.getSecurityToken();
			u.removeAllHeaderParameters();
			var h = this.getOwnerComponent().getModel("opportDisplay").getProperty("/Guid");
			var n = h.replace(/-/g, '');
			var c = new sap.m.UploadCollectionParameter({
				name: "slug",
				value: f.name + ";" + "ServiceReqMain" + ";" + n
			});
			u.addHeaderParameter(c);
			var C = new sap.m.UploadCollectionParameter({
				name: "x-csrf-token",
				value: t
			});
			u.addHeaderParameter(C);
			var o = new sap.m.UploadCollectionParameter({
				name: "content-disposition",
				value: "attachment; filename=\"" + encodeURIComponent(f.name) + "\""
			});
			u.addHeaderParameter(o);
			if (!f.type) {
				var a;
				var F = f.name.split(".");
				var b = "";
				// if (F.length) b = F[F.length - 1];
				if (b && (b.toUpperCase() === "RAR" || b.toUpperCase() === "LOG")) {
					a = new sap.m.UploadCollectionParameter({
						name: "content-type",
						value: "application/octet-stream"
					});
				} else {
					a = new sap.m.UploadCollectionParameter({
						name: "content-type",
						value: "multipart/mixed"
					});
				}
				u.addHeaderParameter(a);
			}
		},

		onUploadCompleted: function(oEvent) {
			this._refreshAttachments();
		},

		_refreshAttachments: function() {
			var oDataModel = this.getOwnerComponent().getModel();
			var oDetailModel = this.getOwnerComponent().getModel("opportDisplay");
			var sGuid = oDetailModel.getProperty("/Guid");
			var sPath = "/DemoRequestSet(guid'" + sGuid + "')";
			oDataModel.read(sPath, {
				urlParameters: {
					"$expand": "NOTES,APPOINTMENT,ATTENDEES,MALIYET,SURVEYS,ATTACHMENT",
					"$select": "ATTACHMENT"
				},
				success: function(resp) {
					oDetailModel.setProperty("/ATTACHMENT", resp.ATTACHMENT);
				},
				error: function(err) {
					MessageToast.show("Belge Yuklenemedi \n" + err.message);
				}
			});
		},

		onFileDeleted: function(oEvent) {
			var oDeletedAttach = oEvent.getParameter("item");
			var sUrl = oDeletedAttach.getUrl();
			var sNewUrl = sUrl.split("/ZCRM_DEMO_REQUEST_SRV")[sUrl.split("/ZCRM_DEMO_REQUEST_SRV").length - 1];
			var oDataModel = this.getOwnerComponent().getModel();
			var oController = this;
			oDataModel.remove(sNewUrl, {
				success: function(resp) {
					oController._refreshAttachments();
				},
				error: function(err) {}
			});
		},
			onFileRenamed: function(oEvent) {
			var oDeletedAttach = oEvent.getParameter("item");
			var sNewFileName = oEvent.getParameter("fileName");
			var sUrl = oDeletedAttach.getUrl();
			var sNewUrl = sUrl.split("/ZCRM_DEMO_REQUEST_SRV")[sUrl.split("/ZCRM_DEMO_REQUEST_SRV").length - 1];
			var oDataModel = this.getOwnerComponent().getModel();
			var oNewDataModel = new sap.ui.model.odata.v2.ODataModel(oDataModel.sServiceUrl, {
				useBatch: false,
				defaultUpdateMethod: "PUT"
			});

			var oController = this;

		},
		
		onNavBackButtonPress : function () {
			if (sap.ui.Device.system.phone) {
				this._goToMainPage();
			}
		}
	});

});