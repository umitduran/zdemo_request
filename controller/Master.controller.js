sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/util/Export",
	"sap/ui/core/util/ExportTypeCSV",
	"borusan/demo/model/formatter",
	"borusan/demo/model/models",
	"sap/m/MessageBox"
], function(Controller, History, Filter, FilterOperator, MessageToast, JSONModel, Export, ExportTypeCSV, formatter, models, MessageBox) {
	"use strict";

	return Controller.extend("borusan.demo.controller.Master", {

		onInit: function() {
			var oView = this.getView();
			var oComp = this.getOwnerComponent();
			oView.addStyleClass(oComp.getContentDensityClass());

			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);

			var oMasterModel = this.getOwnerComponent().getModel("master");
			if (!oMasterModel) {
				var oDetailModel = models.createJSONModel();
				this.getOwnerComponent().setModel(oDetailModel, "master");
				oMasterModel = this.getOwnerComponent().getModel("master");
			}

			var oDataModel = this.getOwnerComponent().getModel();
		},

		_onRouteMatched: function(oEvent) {
			var sRouteName = oEvent.getParameter("name");
			if (sRouteName === "master") {
				var oHistory = History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();
				if (sPreviousHash) {
					var oServiceList = this.getView().byId("idMasterList");
					if (!sap.ui.Device.system.phone) {
						this._displayDetailPage(oServiceList);
					} else {
						this._onUpdateMasterBinding();
					}
				}
			}
		},

		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},

		_onUpdateMasterBinding: function() {
			var oList = this.byId("idMasterList");
			if (oList) {
				var aFilter = oList.getBindingInfo("items").filters;
				var oBindingInfo = oList.getBindingInfo("items");
				oList.bindAggregation("items", {
					path: oBindingInfo.path,
					template: oBindingInfo.template,
					filters: aFilter,
					templateShareable: 1
				});
			}
		},

		onFooterFilterBtnPress: function() {
			this.getView().byId("idMasterApp").to(this.createId("idMasterFilterPage"));
		},

		onFilterNAvBtnPress: function() {
			this.getView().byId("idMasterApp").back();
		},

		_getDomainValues: function(oDataModel, oJsonModel, sPropertyName, mParameters) {
			var aFilters = [];
			if (mParameters.DomName) {
				var oDomainNameFilter = new Filter({
					path: 'DomName',
					operator: FilterOperator.EQ,
					value1: mParameters.DomName
				});
				aFilters.push(oDomainNameFilter);
			}
			if (mParameters.FieldName) {
				var oFieldNameFilter = new Filter({
					path: 'FieldName',
					operator: FilterOperator.EQ,
					value1: mParameters.FieldName
				});
				aFilters.push(oFieldNameFilter);
			}
			oDataModel.read("/CustomizingDomainSHSet", {
				filters: aFilters,
				success: function(resp) {
					// resp.results.unshift({});
					oJsonModel.setProperty(sPropertyName, resp.results);
				},
				error: function(err) {

				}
			});
		},

		_getStatusList: function(oDataModel, oJsonModel, sProcessType) {
			var aFilters = [];
			var oProcessTypeFilter = new Filter({
				path: 'ProcessType',
				operator: FilterOperator.EQ,
				value1: sProcessType
			});
			aFilters.push(oProcessTypeFilter);
			oDataModel.read("/CustomizingStatusListSet", {
				filters: aFilters,
				success: function(resp) {
					oJsonModel.setProperty("/StatusListSet", resp.results);
				},
				error: function(err) {

				}
			});
		},

		onFooterFilterResetBtnPress: function(oEvent) {
			this._resetMasterData();

			MessageToast.show("Filtreler Sifirlandi");
			this.getView().byId("idMasterApp").back();
		},

		onFooterAddBtnPress: function(oEvent) {
			this.getRouter().navTo("opportcreate");
		},

		onListItemPress: function(oEvent) {
			var oSource = oEvent.getSource();
			var oBindingContext = oSource.getBindingContext();
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedItem = oModel.getProperty(sPath);
			if (oSelectedItem) {
				this._navToDetail(oSelectedItem.Guid);
			}
		},

		onMasterListSelectionChange: function(oEvent) {
			var oBindingContext = oEvent.getParameter("listItem").getBindingContext();
			var oModel = oBindingContext.getModel();
			var sPath = oBindingContext.getPath();
			var oSelectedItem = oModel.getProperty(sPath);
			if (oSelectedItem) {
				this._navToDetail(oSelectedItem.Guid);
			}
		},

		_handleInfoBarVisibility: function(bVisible) {
			var oList = this.byId("idMasterList");
			var oInfoToolbar = oList.getInfoToolbar();
			if (oInfoToolbar) {
				oInfoToolbar.setVisible(bVisible);
			}
		},

		handleMasterInfobarPress: function(oEvent) {
			this._handleInfoBarVisibility(false);

			this._resetMasterData();
		},

		_resetMasterData: function() {
			var oMasterModel = this.getOwnerComponent().getModel("master");
			var oMasterData = oMasterModel.getData();

			delete oMasterData.Description;
			delete oMasterData.ObjectId;
			delete oMasterData.PartnerNo;
			delete oMasterData.PartnerFullname;
			delete oMasterData.SerialNo;

			oMasterModel.setData(oMasterData);

			this._handleInfoBarVisibility(false);

			var oList = this.byId("idMasterList");
			var oBindingInfo = oList.getBindingInfo("items");
			oList.bindAggregation("items", {
				path: oBindingInfo.path,
				template: oBindingInfo.template,
				templateShareable: 1
			});
		},

		_navToDetail: function(sGuid) {
			this.getView().getModel("demo").setProperty("/ServiceMode", true);
			this.getRouter().navTo("opportdisplay", {
				objectguid: sGuid
			});
		},

		_navToNoData: function() {
			var oRouter = this.getRouter();
			oRouter.navTo("nodata", true);
		},

		onServiceListUpdated: function(oEvent) {
			// this.getView().setBusy(false);
			var sLength = this.byId("idMasterList").getBinding("items").getLength();
			var sPageTitle = this.getView().getModel("i18n").getProperty("MASTER_MAIN_PAGE_TITLE") + "(" + sLength + ")";
			this.getView().getModel("demo").setProperty("/MasterPageCount", sPageTitle);
			var sReason = oEvent.getParameter("reason");
			if (!sap.ui.Device.system.phone) {
				var oServiceList = oEvent.getSource();
				var oHistory = History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				var oDetailModel = this.getOwnerComponent().getModel("opportDisplay");
				var sGuid = "";
				if (oDetailModel) {
					sGuid = oDetailModel.getProperty("/Guid");
				}

				if (!sPreviousHash && (!sGuid) && sReason && sReason === "Refresh") {
					this._displayDetailPage(oServiceList);

				}
			}
		},

		onServiceListUpdateStarted: function(oEvent) {

			var oMasterModel = this.getOwnerComponent().getModel("Master");
			if (oMasterModel) {
				var bDoUpdate = oMasterModel.getProperty("/DoUpdate");
				if (bDoUpdate) {
					var oList = this.byId("idMasterList");
					var oBindingInfo = oList.getBindingInfo("items");
					oMasterModel.setProperty("/DoUpdate", false);
					oList.bindAggregation("items", oBindingInfo);
				}
			}
		},

		_displayDetailPage: function(oList) {
			var aList = oList.getItems();
			if (aList && aList.length > 0) {
				var oFirstListItem = aList[0];
				var oBindingContext = oFirstListItem.getBindingContext();
				var oModel = oBindingContext.getModel();
				var sContextPath = oFirstListItem.getBindingContextPath();
				var oFirstListItemData = oModel.getProperty(sContextPath);
				var sGuid = oFirstListItemData.Guid;
				this._navToDetail(sGuid);
			} else {
				this._navToNoData();
			}
		},
		onOpportSearch: function(oEvent) {
			var sQuery = oEvent.getParameter("query");
			var oList = this.byId("idMasterList");
			var oBindingInfo = oList.getBindingInfo("items");
			var oMasterModel = this.getOwnerComponent().getModel("master");

			var sMyOrder = oMasterModel.getProperty("/MyOrders");
			var aFilters = [];
			if (sQuery) {
				var oQueryFilter = new Filter({
					path: 'ObjectId',
					operator: FilterOperator.EQ,
					value1: sQuery
				});
				aFilters.push(oQueryFilter);
			}

			if (sMyOrder && sMyOrder.length > 0) {
				var oMyOrder = new sap.ui.model.Filter({
					path: "MyOrders",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sMyOrder
				});
				aFilters.push(oMyOrder);
			}

			this._handleInfoBarVisibility(aFilters.length > 0);

			oList.bindAggregation("items", {
				path: oBindingInfo.path,
				template: oBindingInfo.template,
				templateShareable: 1,
				filters: aFilters
			});
		},

		_handleError: function(err, oController) {
			var sMessage = "";
			var aErrorDetails = JSON.parse(err.responseText).error.innererror.errordetails;
			if (aErrorDetails.length > 0) {
				for (var i = 0; i < aErrorDetails.length; i++) {
					var oError = aErrorDetails[i];
					if (oError.severity === "error") {
						sMessage += (oError.message + "\n");
					}
				}
				if (!sMessage) {
					for (var i = 0; i < aErrorDetails.length; i++) {
						var oError = aErrorDetails[i];
						if (oError.severity === "info") {
							sMessage += (oError.message + "\n");
						}
					}
				}
			} else {
				sMessage = JSON.parse(err.responseText).error.message.value;
			}
			sap.m.MessageBox.error(sMessage, {
				title: oController.getOwnerComponent().getModel("i18n").getProperty("ERROR_TITLE")
			});
		},
		onFooterExportBtnPress: function(oEvent) {
			var oList = this.getView().byId("idMasterList");
			var aList = oList.getItems();
			var oFirstListItem = aList[0];
			var oBindingContext = oFirstListItem.getBindingContext();
			var oModel = oBindingContext.getModel();
			var o18nModel = this.getView().getModel("i18n");
			var oNewJsonModel = new JSONModel();
			oNewJsonModel.setData({});
			var aDemoRequests = [];
			jQuery.each(oModel.oData, function(key, el) {
				if (key && key.indexOf("DemoRequestSet") !== -1) {
					aDemoRequests.push(el);
				}
			});
			oNewJsonModel.setProperty("/DemoRequestSet", aDemoRequests);

			var oExport = new Export({

				exportType: new ExportTypeCSV({
					fileExtension: "csv",
					separatorChar: ";"
				}),

				models: oNewJsonModel,

				rows: {
					path: "/DemoRequestSet"
				},
				columns: [{
					name: o18nModel.getProperty("OBJECT_ID"),
					template: {
						content: "{ObjectId}"
					}
				}, {
					name: o18nModel.getProperty("PARTNET_FULL_NAME"),
					template: {
						content: "{PartnerFullname}"
					}
				}, {
					name: o18nModel.getProperty("STATUS"),
					template: {
						content: "{StatTxt}"
					}
				}, {
					name: o18nModel.getProperty("DESCRIPTION"),
					template: {
						content: "{Description}"
					}
				}]
			});
			oExport.saveFile("Demo Listesi").catch(function(oError) {

			}).then(function() {
				oExport.destroy();
			});
		},
		onFooterFilterSearchBtnPress: function(oEvent) {
			MessageToast.show("Araniyor");
			this.getView().byId("idMasterApp").back();

			var oList = this.byId("idMasterList");
			var oBindingInfo = oList.getBindingInfo("items");
			//var aFilters = [];
			var oMaster = this.getView().getModel("master");

			oList.bindAggregation("items", {
				path: oBindingInfo.path,
				template: oBindingInfo.template,
				templateShareable: 1,
				filters: this._onGetFilters(oMaster)
			});
		},

		_onGetFilters: function(oJson) {
			var aFilter = [];
			var sObjectID = oJson.getProperty("/ObjectId");

			var sDesc = oJson.getProperty("/Description");
			var sPartner = oJson.getProperty("/PartnerNo");
			var sPartnerName = oJson.getProperty("/PartnerFullname");
			var sSerial = oJson.getProperty("/SerialNo");
			var sMyOrder = oJson.getProperty("/MyOrders");

			if (sObjectID && sObjectID.length > 0) {
				var oObjectID = new sap.ui.model.Filter({
					path: "ObjectId",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sObjectID
				});
				aFilter.push(oObjectID);
			}
			if (sDesc && sDesc.length > 0) {
				var oDesc = new sap.ui.model.Filter({
					path: "ObjectId", // Duzeltilecekmis
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sDesc
				});
				aFilter.push(oDesc);
			}
			if (sPartner && sPartner.length > 0) {
				var oPartner = new sap.ui.model.Filter({
					path: "PartnerNo",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sPartner
				});
				aFilter.push(oPartner);
			}
			if (sPartnerName && sPartnerName.length > 0) {
				var oPartnerName = new sap.ui.model.Filter({
					path: "PartnerFullname",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sPartnerName
				});
				aFilter.push(oPartnerName);
			}
			if (sSerial && sSerial.length > 0) {
				var oSerial = new sap.ui.model.Filter({
					path: "SerialNo",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sSerial
				});
				aFilter.push(oSerial);
			}
			if (sMyOrder && sMyOrder.length > 0) {
				var oMyOrder = new sap.ui.model.Filter({
					path: "MyOrders",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: sMyOrder
				});
				aFilter.push(oMyOrder);
			}

			return aFilter;
		},

		onSwitchFilter: function(e) {

			var oMasterModel = this.getOwnerComponent().getModel("master");

			if (e.getParameters().state) {
				var sState = "";
			} else {
				sState = "X";
			}
			this.byId("idPageMAster").setBusy(true);

			oMasterModel.setProperty("/MyOrders", sState);

			var oList = this.byId("idMasterList");
			var oBindingInfo = oList.getBindingInfo("items");
			var aFilter = [];
			var oSwitch = new sap.ui.model.Filter({
				path: "MyOrders",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: sState
			});
			aFilter.push(oSwitch);

			oList.bindAggregation("items", {
				path: oBindingInfo.path,
				template: oBindingInfo.template,
				templateShareable: 1,
				filters: aFilter
			});
			this.byId("idPageMAster").setBusy(false);
		}

	});
});